<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top static-top" id="mainNav">
    <a class="navbar-brand" href="<?php echo site_url('');?>">KuKviz</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('');?>">Početna</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('about');?>">O nama</a>
            </li>
        </ul>
    </div>
</nav>