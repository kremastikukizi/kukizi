<ul class="navbar-nav navbar-sidenav" id="navSidebar">
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Početna">
        <a class="nav-link" href="<?php echo site_url($person->getType()); ?>">
        <i class="fa fa-home"></i><span class="nav-link-text ml-2">Početna</span></a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Korisnici">
        <a class="nav-link" href="<?php echo site_url($person->getType().'/users'); ?>">
        <i class="fa fa-user"></i><span class="nav-link-text ml-2">Korisnici</span></a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kategorije">
        
        <?php
            if ($person->isAdmin()) {
                echo "<a class=\"nav-link nav-link-collapse collapsed\" data-toggle=\"collapse\" href=\"#collapseKategorije\" data-parent=\"#navSidebar\">";
                echo "<i class=\"fa fa-list-ul\"></i><span class=\"nav-link-text ml-2\">Kategorije</span></a>";
                echo "<ul class=\"sidenav-second-level collapse\" id=\"collapseKategorije\">";
                echo "<li><a href=\"".site_url($person->getType().'/categories')."\">Sve kategorije</a></li>";
                echo "<li><a href=\"".site_url($person->getType().'/categories/add')."\">Dodaj novu kategoriju</a></li></ul>";
            }
            else {
                echo "<a class=\"nav-link\" href=\"".site_url($person->getType().'/categories')."\">";
                echo "<i class=\"fa fa-list-ul\"></i><span class=\"nav-link-text ml-2\">Kategorije</span></a>";
            }
        ?>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Pitanja">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapsePitanja" data-parent="#navSidebar">
        <i class="fa fa-question-circle"></i><span class="nav-link-text ml-2">Pitanja</span></a>
        <ul class="sidenav-second-level collapse" id="collapsePitanja">
            <li><a href="<?php echo site_url($person->getType().'/questions');?>">Sva pitanja</a></li>
            <li><a href="<?php echo site_url($person->getType().'/questions/add');?>">Dodaj novo pitanje</a></li>
        </ul>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Korisnici">
        <a class="nav-link" href="<?php echo site_url('game/choose_category'); ?>">
        <i class="fa fa-gamepad"></i><span class="nav-link-text ml-2">Igra</span></a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Prijatelji">
        <a class="nav-link" href="<?php echo site_url('friend/my_friends'); ?>">
        <i class="fa fa-users"></i><span class="nav-link-text ml-2">Prijatelji</span></a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dodaj prijatelja">
        <a class="nav-link" href="<?php echo site_url('friend/add_friend'); ?>">
        <i class="fa fa-plus-circle"></i><span class="nav-link-text ml-2">Dodaj prijatelja</span></a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Rang lista">
        <a class="nav-link" href="<?php echo site_url('scoreboard'); ?>">
        <i class="fa fa-trophy"></i><span class="nav-link-text ml-2">Rang lista</span></a>
    </li>
</ul>
<ul class="navbar-nav sidenav-toggler">
    <li class="nav-item">
        <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
        </a>
    </li>
</ul>