<ul class="navbar-nav navbar-sidenav" id="navSidebar">
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Početna">
        <a class="nav-link" href="<?php echo site_url('player'); ?>">
        <i class="fa fa-home"></i><span class="nav-link-text ml-2">Početna</span></a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Korisnici">
        <a class="nav-link" href="<?php echo site_url('game/choose_category'); ?>">
        <i class="fa fa-gamepad"></i><span class="nav-link-text ml-2">Igra</span></a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Prijatelji">
        <a class="nav-link" href="<?php echo site_url('friend/my_friends'); ?>">
        <i class="fa fa-users"></i><span class="nav-link-text ml-2">Prijatelji</span></a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dodaj prijatelja">
        <a class="nav-link" href="<?php echo site_url('friend/add_friend'); ?>">
        <i class="fa fa-plus-circle"></i><span class="nav-link-text ml-2">Dodaj prijatelja</span></a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Rang lista">
        <a class="nav-link" href="<?php echo site_url('scoreboard'); ?>">
        <i class="fa fa-trophy"></i><span class="nav-link-text ml-2">Rang lista</span></a>
    </li>
</ul>
<ul class="navbar-nav sidenav-toggler">
    <li class="nav-item">
        <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
        </a>
    </li>
</ul>