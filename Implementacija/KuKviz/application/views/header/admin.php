<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="<?php echo site_url('');?>">KuKviz</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <?php
            if(current_url() != site_url($person->getType())) {
                $this->load->view('header/admin-sidebar');
            }
        ?>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user-plus"></i>
                    <span class="d-lg-none">Pozivi za prijateljstvo
                        <span class="badge badge-pill badge-success"></span>
                    </span>
                    <?php 
                        $myFriends = $person->getFriends();
                        $friendsWithMe = $person->getFriendsWithMe();
                        $requests = array();
                        foreach($friendsWithMe as $friendWithMe){
                            $new = true;
                            foreach ($myFriends as $myFriend){
                                if ($myFriend == $friendWithMe){
                                    $new = false;
                                    break;
                                }
                            }
                            if ($new) {
                                $requests[] = $friendWithMe;
                            }
                        }
                        $circleFriends = "";
                        if (count($requests) > 0){
                            $circleFriends = "<span class=\"indicator text-success d-none d-lg-block\">
                                                <i class=\"fa fa-fw fa-circle\"></i>
                                            </span>";
                        }
                        echo $circleFriends;
                    ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="messagesDropdown">
                     <h6 class="dropdown-header">
                        <?php
                            $messageFriends = "Nema novih zahteva za prijateljstvo";
                            if (count($requests) > 0){
                                $messageFriends = "Novi zahtevi za prijateljstvo:";
                            }
                            echo $messageFriends;
                        ?>
                    </h6>
                    <!-- PHP code -->
                    <?php                    
                    foreach ($requests as $request){
                        $messageFriends = "<div class=\"dropdown-divider\"></div>
                                            <div style=\"padding: .25rem 1.5rem; white-space: nowrap;\">
                                                <span class=\"text-secondary\">
                                                    <h6>Zahtev od strane korisnika: ".$request->getUsername()."</h6>
                                                </span>
                                                <div class=\"dropdown-message small\">
                                                    <div class=\"row\">
                                                        <div class=\"col-sm\">
                                                            <a href=\"".site_url("Friend\accept\\".$request->getId())."\" class=\"btn btn-block btn-success\">Prihvati</a>            
                                                        </div>
                                                        <div class=\"col-sm\">
                                                            <a href=\"".site_url("Friend\decline\\".$request->getId())."\" class=\"btn btn-block btn-danger\">Odbij</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                        echo $messageFriends;
                    }
                    ?>
                </div>
            </li>
            
            
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-flag"></i>
                    <span class="d-lg-none">Prijave
                        <span class="badge badge-pill badge-danger"><!-- PHP code --></span>
                    </span>
                    <?php 
                        $reports = $this->doctrine->em->getRepository('Entity\Report')->findAll();
                        $my = 0;
                        $myReports = array();
                        foreach($reports as $report){
                            if ($report->getQuestion()->getModerator()->getId() == $person->getId()){
                                if ($report->isHandled() == false){
                                    $myReports[] = $report;
                                }
                            }
                        }
                        $circleReport = "";
                        if (count($myReports) > 0){
                            $circleReport = "<span class=\"indicator text-danger d-none d-lg-block\">
                                        <i class=\"fa fa-fw fa-circle\"></i>
                                    </span>";
                        }
                        echo $circleReport;
                    ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="alertsDropdown">
                    <h6 class="dropdown-header">
                        <?php
                            $messageReport = "Nema novih prijava";
                            if (count($myReports) > 0){
                                $messageReport = "Nove prijave:";
                            }
                            echo $messageReport;
                        ?>
                    </h6>
                    <!-- PHP code -->
                    <?php
                    
                    foreach ($myReports as $report){
                        $messageReport = "<div class=\"dropdown-divider\"></div>
                                    <a class=\"dropdown-item\" href=\"".site_url("Report\index\\".$report->getId())."\">
                                        <strong>Pitanje ".$report->getQuestion()->getId()." je prijavljeno</strong>                                        
                                        <div class=\"dropdown-message small\">Pitanje: ".$report->getQuestionText()." prijavljeno je od strane korisnika ".$report->getPersonUsername()."</div>
                                    </a>";
                        echo $messageReport;
                    }                 
                    ?>
            </li>
            
            
            
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-play-circle"></i>
                    <span class="d-lg-none">Pozivi za igru
                        <span class="badge badge-pill badge-primary"></span>
                    </span>
                    <?php
                        $scores = $person->getUnplayedScores();
                        $circleScores = "";
                        if (count($scores) > 0){
                            $circleScores = "<span class=\"indicator text-primary d-none d-lg-block\">
                                                <i class=\"fa fa-fw fa-circle\"></i>
                                            </span>";
                        }
                        echo $circleScores;
                    ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="messagesDropdown">
                     <h6 class="dropdown-header">
                        <?php
                            $messageScores = "Nema novih poziva za igru";
                            if (count($scores) > 0){
                                $messageScores = "Novi pozivi za igru:";
                            }
                            echo $messageScores;
                        ?>
                    </h6>
                    <!-- PHP code -->
                    <?php
                    foreach ($scores as $score){
                        $messageScores = "<div class=\"dropdown-divider\"></div>
                                            <div style=\"padding: .25rem 1.5rem; white-space: nowrap;\">
                                                <span class=\"text-secondary\">
                                                    <h6>Poziv od strane korisnika: ".$score->getSession()->getPersonUsername()."</h6>
                                                </span>
                                                <div class=\"dropdown-message small\">
                                                    <div class=\"row\">
                                                        <div class=\"col-sm\">
                                                            <a href=\"".site_url("Game\acceptInvite\\".$score->getId())."\" class=\"btn btn-block btn-success\">Prihvati</a>            
                                                        </div>
                                                        <div class=\"col-sm\">
                                                            <a href=\"".site_url("Game\declineInvite\\".$score->getId())."\" class=\"btn btn-block btn-danger\">Odbij</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                        echo $messageScores;
                    }
                    ?>
                </div>
            </li>
            
            
            
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('profile'); ?>">
                    <span class="nav-link-text mr-2">Dobrodošao, <?php echo $person->getName(); ?></span>
                    <i class="fa fa-user"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#logoutModal">
                    <span class="nav-link-text mr-1">Odjava</span>
                    <i class="fa fa-fw fa-sign-out"></i>
                </a>
            </li>
        </ul>
    </div>
</nav>