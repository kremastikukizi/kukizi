<div class="modal fade" id="nameModal" tabindex="-1" role="dialog" aria-labelledby="nameModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form name="form-name" method="POST" action="<?php echo site_url("Person\changeName") ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="nameModalLabel">Promena imena</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body my-3">
                    <h6>Unesite Vaše novo ime</h6>
                    <input class="form-control" type="text" id="newname" name="newname">
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-primary">Promeni</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="genderModal" tabindex="-1" role="dialog" aria-labelledby="genderModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form name="form-gender" method="POST" action="<?php echo site_url("Person\changeGender") ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="genderModalLabel">Promena pola</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body my-3">
                    <h6>Da li ste sigurni da želite da promenite Vaš pol?</h6>
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-primary">Promeni</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="usernameModal" tabindex="-1" role="dialog" aria-labelledby="usernameModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form name="form-username" method="POST" action="<?php echo site_url("Person\changeUsername") ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="usernameModalLabel">Promena korisničkog imena</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body my-3">
                    <h6>Unesite Vaše novo korisničko ime</h6>
                    <input class="form-control" type="text" id="newusername" name="newusername">
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-primary">Promeni</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form name="form-password" method="POST" action="<?php echo site_url("Person\changePassword") ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="passwordModalLabel">Promena lozinke</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body my-3">
                    <h6>Unesite Vašu novu lozinku</h6>
                    <input class="form-control" type="password" id="newpassword" name="newpassword">
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-primary">Promeni</button>
                </div>
            </form>
        </div>
    </div>
</div>

 <div class="modal fade" id="mailModal" tabindex="-1" role="dialog" aria-labelledby="mailModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form name="form-mail" method="POST" action="<?php echo site_url("Person\changeEmail") ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="mailModalLabel">Promena e-maila</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body my-3">
                    <h6>Unesite Vaš novi e-mail</h6>
                    <input class="form-control" type="email" id="newmail" name="newmail">
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-primary">Promeni</button>
                </div>
            </form>
        </div>
    </div>
</div>