<div class="modal fade" id="exitModal" tabindex="-1" role="dialog" aria-labelledby="exitModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form name="form-exit" method="POST" action="<?php echo site_url("Person/exitGame") ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exitModalLabel">Izlaz iz partije</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <br/><h4>Da li sigurno želite da napustite partiju?</h4><br/>
                </div>
                <div class="modal-footer text-center">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Odustani</button>
                    <button type="submit" class="btn btn-primary">Izađi</button>
                </div>
            </form>
        </div>
    </div>
</div>