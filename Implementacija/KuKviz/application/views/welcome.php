<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Aleksa">
        <title>KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom style for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/content-center.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/welcome'); ?>
        <!-- End of header -->
        
        <div class="container-fluid content-center">
            <!-- Content -->
            <div class="text-center">
                <h1 class="">KuKviz</h1><br/>
                <p class="lead">KuKviz je jedan od popularnijih on-line kvizova. Dođite i pridružite nam se u rešavanju zanimljivih pitanja iz velikog broja različitih kategorija!</p>
                <p class="lead"><br/></p>
                <div class="row">
                    <div class="col-md"></div>
                    <div class="col-md text-center">
                        <a href="<?php echo site_url('login') ?>" class="btn btn-lg btn-primary btn-block">Prijavi se</a>
                        <a href="<?php echo site_url('register') ?>" class="btn btn-lg btn-primary btn-block">Registruj se </a>
                        <a href="<?php echo site_url('game/choose_category') ?>" class="btn btn-lg btn-primary btn-block">Igraj kao gost</a>
                    </div>
                    <div class="col-md"></div>
                </div>
            </div>
            <!-- End of content -->
            
            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    </body>
</html>