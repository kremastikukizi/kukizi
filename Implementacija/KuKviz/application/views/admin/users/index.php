<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Petar">
        <title>Korisnici - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Page level plugin CSS-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/datatables/css/dataTables.bootstrap4.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php 
            $this->load->view('header/admin');
            $dataTableName = "dataTable";
            if ($person->isModerator()) {
                $dataTableName = "dataTableModerator";
            }
        ?>
        <!-- End of header -->
        
        <div class="content-wrapper">
            <!-- Content -->
            <div class="container-fluid">
                <div class="card mb-3">
                    <div class="card-header text-left"><i class="fa fa-user"></i> Svi korisnici</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="<?php echo $dataTableName;?>" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tip</th>
                                        <th>Ime</th>
                                        <th>Prezime</th>
                                        <th>Korisničko ime</th>
                                        <th>E-mail</th>
                                        <?php
                                            if ($person->isAdmin()) {
                                                echo "<th></th><th></th><th></th>";
                                            }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($users as $user) {
                                            echo "<tr>";
                                            echo "<td>".$user->getId()."</td>";

                                            echo "<td>".$user->typeToString()."</td>";
                                            echo "<td>".$user->getName()."</td>";
                                            echo "<td>".$user->getName()."</td>";
                                            echo "<td>".$user->getUsername()."</td>";
                                            echo "<td>".$user->getEmail()."</td>";
                                            if($person->isAdmin()) {
                                                
                                                if ($user->isAdmin()) {
                                                    echo "<td style=\"text-align: center;\"><i style=\"color: grey;\" class=\"fa fa-plus-circle\"></i></td>".
                                                    "<td style=\"text-align: center;\"><i style=\"color: grey;\" class=\"fa fa-minus-circle\"></i></td>".
                                                    "<td style=\"text-align: center;\"><i style=\"color: grey;\" class=\"fa fa-trash\"></i></td>";
                                                }
                                                else if ($user->isModerator()) {
                                                    echo "<td style=\"text-align: center;\"><a href=\"".site_url('Admin/upgradePerson/'.$user->getId())."\"><i style=\"color: green;\" class=\"fa fa-plus-circle\"></i></a></td>".
                                                    "<td style=\"text-align: center;\"><a href=\"".site_url('Admin/downgradePerson/'.$user->getId())."\"><i style=\"color: red;\" class=\"fa fa-minus-circle\"></i></a></td>".
                                                    "<td style=\"text-align: center;\"><a href=\"".site_url('Admin/deletePerson/'.$user->getId())."\"><i style=\"color: brown;\" class=\"fa fa-trash\"></i></a></td>";
                                                }
                                                else {
                                                    echo "<td style=\"text-align: center;\"><a href=\"".site_url('Admin/upgradePerson/'.$user->getId())."\"><i style=\"color: green;\" class=\"fa fa-plus-circle\"></i></a></td>".
                                                    "<td style=\"text-align: center;\"><i style=\"color: grey;\" class=\"fa fa-minus-circle\"></i></td>".
                                                    "<td style=\"text-align: center;\"><a href=\"".site_url('Admin/deletePerson/'.$user->getId())."\"><i style=\"color: brown;\" class=\"fa fa-trash\"></i></a></td>";
                                                }
                                                
                                            }
                                            echo "</tr>";
                                        }
                                    ?>
                              </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer small text-muted text-left">Kremasti kukizi</div>
                </div>
            </div>
            <!-- End of content -->

            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <?php $this->load->view('footer/logout'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?php echo base_url('vendor/datatables/js/jquery.dataTables.js'); ?>"></script>
        <script src="<?php echo base_url('vendor/datatables/js/dataTables.bootstrap4.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/dashboard.js'); ?>"></script>
        <script src="<?php echo base_url('js/admin/users.js'); ?>"></script>
    </body>
</html>

