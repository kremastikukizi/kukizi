<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Petar">
        <title>Kategorije - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Page level plugin CSS-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/datatables/css/dataTables.bootstrap4.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php 
            $this->load->view('header/admin');
            $dataTableName = "dataTable";
            if ($person->isModerator()) {
                $dataTableName = "dataTableModerator";
            }
        ?>
        <!-- End of header -->
        
        <div class="content-wrapper">
            <!-- Content -->
            <div class="container-fluid">
                <div class="card mb-3">
                    <div class="card-header">
                        <div style="float:left"><i class="fa fa-list-ul"></i> Sve kategorije</div>
                        <!--<div style="float:right"><a class="btn btn-dark" href="<?php //echo site_url('admin/categories/add'); ?>">Dodaj novu kategoriju</a></div>-->
                        <div style="clear:both"></div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="<?php echo $dataTableName;?>" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Ime kategorije</th>
                                        <th>Ukupan broj pitanja</th>
                                        <?php
                                            if ($person->isAdmin()) {
                                                echo "<th></th><th></th>";
                                            }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($categories as $category) {
                                            echo "<tr>";
                                            echo "<td>".$category->getId()."</td>";
                                            echo "<td>".$category->getName()."</td>";
                                            echo "<td>".count($category->getQuestions())."</td>";
                                            if ($person->isAdmin()) {
                                                echo
                                                "<td style=\"text-align: center;\"><a href=\"".site_url('Admin/editCategoryView/'.$category->getId())."\"><i class=\"fa fa-edit\"></i></a></td>".
                                                "<td style=\"text-align: center;\"><a href=\"".site_url('Admin/deleteCategory/'.$category->getId())."\"><i style=\"color: brown;\" class=\"fa fa-trash\"></i></a></td>";
                                                echo "</tr>";
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-left">Kremasti kukizi</div>
                </div>
            </div>
            <!-- End of content -->

            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <?php $this->load->view('footer/logout'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?php echo base_url('vendor/datatables/js/jquery.dataTables.js'); ?>"></script>
        <script src="<?php echo base_url('vendor/datatables/js/dataTables.bootstrap4.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/dashboard.js'); ?>"></script>
        <script src="<?php echo base_url('js/admin/categories.js'); ?>"></script>
    </body>
</html>

