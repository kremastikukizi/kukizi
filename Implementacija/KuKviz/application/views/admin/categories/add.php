<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Natalija">
        <title>Dodavanje kategorije - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/admin'); ?>
        <!-- End of header -->
        
        <div class="content-wrapper">
            <!-- Content -->
            <div class="container-fluid">
                <div class="card card-login mx-auto my-3">
                    <div class="card-header">Dodavanje kategorije</div>
                    <div class="card-body">
                        <form name="form-category" method="POST" action="<?php echo site_url('Admin/addCategory') ?>">
                            <?php if(isset($message)) echo "<div class=\"alert alert-danger\">".$message."</div>";?>
                            <div class="form-group">
                                <?php echo form_error("name","<div class=\"alert alert-danger\">","</div>"); ?>
                                <label for="name">Naziv kategorije</label>
                                <input class="form-control" type="text" name="name" placeholder="Unesite naziv kategorije" autofocus>
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Dodajte kategoriju</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End of content -->

            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <?php $this->load->view('footer/logout'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    </body>
</html>