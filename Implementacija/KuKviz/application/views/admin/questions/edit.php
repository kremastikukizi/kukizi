<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Natalija">
        <title>Izmena pitanja - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/admin'); ?>
        <!-- End of header -->
        
        <div class="content-wrapper">
            <!-- Content -->
            <div class="container-fluid">
                <div class="card card-login mx-auto my-3">
                    <div class="card-header">Izmena pitanja</div>
                    <div class="card-body">
                        <form name="form-category" method="POST" action="<?php echo site_url('Admin/editQuestion/'.$question->getId()); ?>">
                            <div class="form-group">
                                <label for="text">Tekst pitanja</label>
                                <input class="form-control" type="text" name="text" placeholder="<?php echo $question->getText();?>" autofocus>
                            </div>
                            <div class="form-group">
                                <label for="answer1">Prvi odgovor:</label>
                                <input class="form-control" type="text" name="answer1" placeholder="<?php echo $question->getAnswers()[0]->getText();?>" autofocus>
                            </div>
                            <div class="form-group">
                                <label for="answer2">Drugi odgovor:</label>
                                <input class="form-control" type="text" name="answer2" placeholder="<?php echo $question->getAnswers()[1]->getText();?>" autofocus>
                            </div>
                            <div class="form-group">
                                <label for="answer3">Treći odgovor:</label>
                                <input class="form-control" type="text" name="answer3" placeholder="<?php echo $question->getAnswers()[2]->getText();?>" autofocus>
                            </div>
                            <div class="form-group">
                                <label for="answer4">Četvrti odgovor:</label>
                                <input class="form-control" type="text" name="answer4" placeholder="<?php echo $question->getAnswers()[3]->getText();?>" autofocus>
                            </div>
                            <div class="form-group">
                                <label for="correct">Tačan odgovor:</label>
                                <select class="form-control" name="correct">
                                    <option value='0'>Izaberite tačan odgovor</option>
                                    <option value='1'>Prvi odgovor</option>
                                    <option value='2'>Drugi odgovor</option>
                                    <option value='3'>Treći odgovor</option>
                                    <option value='4'>Četvrti odgovor</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="category">Kategorija:</label>
                                <select class="form-control" name="category">
                                    <option value='0'>Izaberite kategoriju</option>
                                    <?php
                                        foreach ($categories as $category){
                                            echo "<option value='".$category->getId()."'>".$category->getName()."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Promenite pitanje</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End of content -->

            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <?php $this->load->view('footer/logout'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/dashboard.js'); ?>"></script>
    </body>
</html>