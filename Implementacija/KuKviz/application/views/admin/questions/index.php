<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Petar">
        <title>Pitanja - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Page level plugin CSS-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/datatables/css/dataTables.bootstrap4.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/admin'); ?>
        <!-- End of header -->
        
        <div class="content-wrapper">
            <!-- Content -->
            <div class="container-fluid">
                <div class="card mb-3">
                    <div class="card-header">
                        <div style="float:left"><i class="fa fa-question-circle"></i> Sva pitanja</div>
                        <!--<div style="float:right"><a class="btn btn-dark" href="<?php //echo site_url('admin/questions/add'); ?>">Dodaj novo pitanje</a></div>-->
                        <div style="clear:both"></div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Pitanje</th>
                                        <th>Kategorija</th>
                                        <th>Odgovori</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($questions as $question) {
                                            echo "<tr>";
                                            echo "<td>".$question->getId()."</td>";
                                            echo "<td>".$question->getText()."</td>";
                                            echo "<td>".$question->getCategory()->getName()."</td>";
                                            echo "<td>";
                                            $i = 0;
                                            foreach($question->getAnswers() as $answer) {
                                                if($answer->isCorrect()) echo "<b style=\"color:green;\">";
                                                echo $answer->getText();
                                                if($i++ < 3) echo ", ";
                                                if($answer->isCorrect()) echo "</b>";
                                            }
                                            echo "</td>";
                                            echo 
                                                "<td style=\"text-align: center;\"><a href=\"".site_url('Admin/editQuestionView/'.$question->getId())."\"><i class=\"fa fa-edit\"></i></a></td>".
                                                "<td style=\"text-align: center;\"><a href=\"".site_url('Admin/deleteQuestion/'.$question->getId())."\"><i style=\"color: brown;\" class=\"fa fa-trash\"></i></a></td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-left">Kremasti kukizi</div>
                </div>
            </div>
            <!-- End of content -->

            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <?php $this->load->view('footer/logout'); ?>
            <!-- End of footer -->
        </div>
        
        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?php echo base_url('vendor/datatables/js/jquery.dataTables.js'); ?>"></script>
        <script src="<?php echo base_url('vendor/datatables/js/dataTables.bootstrap4.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/dashboard.js'); ?>"></script>
        <script src="<?php echo base_url('js/admin/questions.js'); ?>"></script>
    </body>
</html>

