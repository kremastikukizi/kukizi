<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Petar">
        <title><?php echo ($person->isAdmin()) ? "Administratorski" : "Moderatorski"; ?> panel - KuKviz</title>

        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Custom style for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
    </head>
    <body class="fixed-nav sticky-footer"  background="<?php echo base_url('images/background.png'); ?>">
        <!-- Header -->
        <?php $this->load->view('header/admin'); ?>
        <!-- End of header -->
        
        <div class="container-fluid mt-5">
            <!-- Content -->
            <div class="row">
                <div class="col text-center">
                    <h1>
                        <?php
                            if ($person->isAdmin()) {
                                echo "Administratorski panel";
                            }
                            else {
                                echo "Moderatorski panel";
                            }
                        ?>
                    </h1>
                    
                </div>
            </div>
            <br/>
            <div class="row mx-5">
                <div class="col-xl m-4">
                    <div class="text-center">
                        <a href="<?php echo site_url($person->getType().'/users') ?>">
                            <img class="card-img-top img-fluid" style="padding: 30px;" src="<?php echo base_url('images/allusers.png'); ?>">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title" style="font-size:36pt;">Korisnici</h5>
                        </div>
                    </div>
                </div>
                <div class="col-xl m-4">
                    <div class="text-center">
                        <a href="<?php echo site_url($person->getType().'/categories') ?>">
                            <img class="card-img-top img-fluid" style="padding: 30px;" src="<?php echo base_url('images/category.png'); ?>">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title" style="font-size:36pt;">Kategorije</h5>
                        </div>
                  </div>
                </div>
                <div class="col-xl m-4">
                    <div class="text-center">
                        <a href="<?php echo site_url($person->getType().'/questions') ?>">
                            <img class="card-img-top img-fluid" style="padding: 30px;" src="<?php echo base_url('images/question.png'); ?>">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title" style="font-size:36pt;">Pitanja</h5>
                        </div>
                  </div>
                </div>
            </div>            
            <div class="row mx-5">
                <div class="col-xl m-4">
                    <div class="text-center">
                        <a href="<?php echo site_url('game/choose_category'); ?>">
                            <img class="card-img-top img-fluid" style="padding: 20px;" src="<?php echo base_url('images/play-game.png'); ?>">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title" style="font-size:26pt;">Igraj novu igru</h5>
                        </div>
                    </div>
                </div>
                <div class="col-xl m-4">
                    <div class="text-center">
                        <a href="<?php echo site_url('friend/my_friends'); ?>">
                            <img class="card-img-top img-fluid" src="<?php echo base_url('images/users.png'); ?>">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title" style="font-size:26pt;">Prijatelji</h5>
                        </div>
                    </div>
                </div>
                <div class="col-xl m-4">
                    <div class="text-center">
                        <a href="<?php echo site_url('friend/add_friend'); ?>">
                            <img class="card-img-top img-fluid" src="<?php echo base_url('images/add-user.png'); ?>">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title" style="font-size:26pt;">Dodaj prijatelja</h5>
                        </div>
                  </div>
                </div>
                <div class="col-xl m-4">
                    <div class="text-center">
                        <a href="<?php echo site_url('scoreboard'); ?>">
                            <img class="card-img-top img-fluid" style="padding: 20px;" src="<?php echo base_url('images/rang-list.png'); ?>">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title" style="font-size:26pt;">Rang lista</h5>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of content -->
                    
            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <?php $this->load->view('footer/logout'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    </body>
</html>