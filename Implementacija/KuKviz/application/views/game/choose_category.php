<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Miljan">
        <title>Izbor kategorija - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/choose_category.css'); ?>">
    </head>
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/game'); ?>
        <!-- End of header -->
        
        <!-- Content -->
        <div class="container-fluid" style="padding: 16px;">
            <div class="card my-3">
                <form class="form-group" style="margin-bottom: 0px;" name="form-choose_category" method="POST" action="<?php echo site_url("Game/chooseCategory"); ?>">
                    <div class="card-header text-left"><i class="fa fa-cog"></i> Podešavanje igre</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md">
                                <div class="container-fluid">
                                    <div class="card mb-3">
                                        <div class="card-header text-left"><i class="fa fa-question"></i> Izbor kategorija</div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-center">
                                                    <h2>Izabrati kategorije</h2>
                                                </div>
                                            </div>
                                            <br/> <br/>
                                            <?php if(isset($message)){ echo "<div class=\"alert alert-danger\">".$message."</div>"; }?>
                                            <div class="table-responsive">
                                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Kategorija</th>
                                                            <th class="text-center">Dodaj</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            foreach($categories as $category) {
                                                                echo "<tr>";
                                                                echo "<td>".$category->getName()."</td>";
                                                                echo "<td class=\"text-center\">";
                                                                echo "<input type=\"checkbox\" class=\"forcat\" name=\"catcb".$category->getId()."\" id=\"catcb".$category->getId()."\">";
                                                                echo "<label class=\"forsize\" for=\"catcb".$category->getId()."\"></label>";
                                                                echo "</td>";
                                                                echo "</tr>";
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $style = "";
                            if ($person == NULL){
                                $style = " style=\"display:none\" ";
                            } ?>
                            <div class="col-md" <?php echo $style; ?>>
                                <div class="container-fluid">
                                    <div class="card mb-3">
                                        <div class="card-header text-left"><i class="fa fa-users"></i> Pozovi prijatelja</div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-center">
                                                    <h2>Prijatelji</h2>
                                                </div>
                                            </div>
                                            <br/> <br/>
                                            <div class="table-responsive">
                                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Korisničko ime</th>
                                                            <th class="text-center">Dodaj</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            if ($person != NULL){
                                                                foreach($person->getFriends() as $user) {
                                                                    if (!$person->getFriendsWithMe()->contains($user)) {
                                                                        continue;
                                                                    }
                                                                    echo "<tr>";
                                                                    echo "<td>".$user->getUsername()."</td>";
                                                                    echo "<td class=\"text-center\">";
                                                                    echo "<input type=\"checkbox\" class=\"forfriend\" name=\"friendcb".$user->getId()."\" id=\"friendcb".$user->getId()."\">";
                                                                    echo "<label class=\"forsize\" for=\"friendcb".$user->getId()."\"></label>";
                                                                    echo "</td>";
                                                                    echo "</tr>";
                                                                }
                                                            }
                                                        ?>
                                                  </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>

                        <div class="row alert alert-info">
                            <div class="col-md-5 text-right mt-1">
                                <h5>Dužina partije</h5>                                       
                            </div>
                            <div class="col-md-2 offset-2 text-center">
                                <select class="form-control" name="length">
                                    <option>Kratka</option>
                                    <option selected>Srednja</option>
                                    <option>Duga</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer small text-muted text-center">
                        <input type="submit" class="btn btn-md btn-success" value="Pokreni partiju">
                    </div>
                </form>
            </div>
        </div>
        <!-- End of content -->



        <!-- Footer -->
        <?php $this->load->view('footer/game'); ?>
        <?php $this->load->view('footer/welcome'); ?>
        <!-- End of footer -->
    
        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/dashboard.js'); ?>"></script>
    </body>    
</html>