<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    if ($this->session->userdata("personId") != NULL){
        $sessionId = $this->session->userdata("session");
        $session = $this->doctrine->em->find("Entity\Session", $sessionId);

        $personId = $this->session->userdata("personId");
        $person = $this->doctrine->em->find("Entity\Person", $personId);

        $score = $session->getScoreForPerson($person);

        if ($score->getIsPlayed()) {
            redirect("Game/end");
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Miljan">
        <title>Kviz - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Timer widget core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/inc/css/TimeCircles.css'); ?>"/>
        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/game.css'); ?>">
    </head>

    <body class="fixed-nav sticky-footer" onload="startTimer(<?php echo count($questions)?>)">
        <!-- Header -->
        <?php $this->load->view('header/game'); ?>
        <!-- End of header -->
        
        <div class="container-fluid mt-5 text-center">
            <!-- Content -->
            <?php $num = 1; ?>
            <div class="row">
                <div class="col-sm-6 mt-4" id="questionNumber"></div>
                <div class="col-sm-6">
                    <div id="CountDownTimer" data-timer="10" style="float: right; width: 500px; height: 125px;"></div>
                </div>
            </div>
            <div class="row mt-5"><div class="col-sm"><br/><br/></div></div>
            <div class="row">
                <div class="col-sm">
                    <form class="form" id="form-game" name="form-game" method="POST" action="<?php echo site_url('game/end'); ?>">
                    <?php 
                        $shuffle = array(0,1,2,3);
                        shuffle($shuffle);
                        $colsize="lg-";
                        $btnclass="class=\"btn btn-primary btn-block\"";
                        foreach($questions as $question){
                            if ($num > count($questions)) { 
                                break;
                            }
                            $style = " style=\"\" ";
                            $submit = "";
                            if ($num != 1){
                                $style = " style=\"display:none\" ";
                            }
                            if ($num == count($questions)){                                        
                                $submit = " type=\"submit\"";
                            }
                            echo"<div id=\"".$num."question\"".$style.">
                                    <h2>".$questions[$num - 1]->getText()."</h2><br/><br/><br/>
                                    <div class=\"row\"> 
                                        <div class=\"col-".$colsize."3\">
                                        </div>
                                        <div class=\"col-".$colsize."3\">
                                            <button".$submit." value=\"1\" name=\"".$num."answer\" ".$btnclass." onclick=\"return put(".$num.", ".($shuffle[0] + 1).")\">".$questions[$num - 1]->getAnswers()[$shuffle[0]]->getText()."</button>
                                        </div>
                                        <div class=\"col-".$colsize."3\">                                  
                                            <button".$submit." value=\"2\" name=\"".$num."answer\" ".$btnclass." onclick=\"return put(".$num.", ".($shuffle[1] + 1).")\">".$questions[$num - 1]->getAnswers()[$shuffle[1]]->getText()."</button>
                                        </div>
                                        <div class=\"col-".$colsize."3\">
                                        </div>
                                    </div> <br/>
                                    <div class=\"row\">
                                        <div class=\"col-".$colsize."3\">
                                        </div>
                                        <div class=\"col-".$colsize."3\">
                                            <button".$submit." value=\"3\" name=\"".$num."answer\" ".$btnclass." onclick=\"return put(".$num.", ".($shuffle[2] + 1).")\">".$questions[$num - 1]->getAnswers()[$shuffle[2]]->getText()."</button>
                                        </div>
                                        <div class=\"col-".$colsize."3\">                                  
                                            <button".$submit." value=\"4\" name=\"".$num."answer\" ".$btnclass." onclick=\"return put(".$num.", ".($shuffle[3] + 1).")\">".$questions[$num - 1]->getAnswers()[$shuffle[3]]->getText()."</button>
                                        </div>  
                                        <div class=\"col-".$colsize."3\">
                                        </div>
                                    </div> <br/>                                            
                                    <div class=\"row\">
                                        <div class=\"col-".$colsize."4\">
                                        </div>
                                        <div class=\"col-".$colsize."4\">
                                            <button".$submit." value=\"5\" name=\"".$num."answer\" ".$btnclass." onclick=\"return put(".$num.", 5)\">Ne znam</button>
                                        </div>
                                        <div class=\"col-".$colsize."4\">
                                        </div>
                                    </div>  
                                    <div class=\"row\" style=\"display: none\">                                                     
                                        <div class=\"col\">                                  
                                            <input type=\"text\" name=\"".$num."answered\" id=\"".$num."answered\">
                                        </div>                           
                                    </div>
                                </div>";
                            $num = $num + 1;
                        }
                    ?>
                        <div class="row" style="display:none">
                            <div class="col">
                                <input type="text" name="sec" id="sec" value="0">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End of content -->
                    
            <!-- Footer -->
            <?php $this->load->view('footer/game'); ?>
            <?php $this->load->view('footer/welcome'); ?>
            <!-- End of footer -->
        </div>
        
        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Timer widget core JavaScript -->
        <script src="<?php echo base_url('vendor/inc/js/TimeCircles.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/game/game.js'); ?>"></script>
    </body>
</html>