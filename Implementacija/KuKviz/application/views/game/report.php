<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Natalija">
        <title>Prijava greške - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/game'); ?>
        <!-- End of header -->
        <!-- Content -->
        <div class="container-fluid">
            <div class="card card-login mx-auto my-3">
                <div class="card-header">Prijava greške</div>
                <div class="card-body">
                    <form name="form-report" method="POST" action="<?php echo site_url('Game/report/'.$question) ?>">
                        <div class="form-group">
                            <label for="name">Tekst prijave:</label>
                            <input class="form-control" type="text" name="reportText" placeholder="Unesite tekst prijave" autofocus>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Prijavi grešku</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- End of content -->
        <!-- Footer -->
        <?php $this->load->view('footer/game'); ?>
        <?php $this->load->view('footer/welcome'); ?>
        <!-- End of footer -->

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/dashboard.js'); ?>"></script>
    </body>
</html>




