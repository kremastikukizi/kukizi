<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Aleksa">
        <title>Kraj igre - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Page level plugin CSS-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/datatables/css/dataTables.bootstrap4.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/game.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/game'); ?>
        <!-- End of header -->
                
        <div class="container-fluid">
            <!-- Content -->
            <div class="card mb-3 mt-5">
                <div class="card-header text-left">
                    <i class="fa fa-table"></i> Rezultat partije
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <?php 
                                $style = "";
                                if ($this->session->userdata("personId") == NULL){
                                    $style = " style=\"display:none\"";
                                }
                            ?>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Pitanje</th>
                                    <th>Odgovor 1</th>
                                    <th>Odgovor 2</th>
                                    <th>Odgovor 3</th>
                                    <th>Odgovor 4</th>
                                    <th class="text-center" <?php echo $style;?>></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if ($this->session->userdata("personId") != NULL){
                                        $questions = $session->getQuestions();
                                        $score = $session->getScoreForPerson($person);                                                                            
                                        $answers = $score->getAnswers();
                                    }
                                    $questionNumber = count($questions);
                                    $points = 0;
                                    $j = 0;
                                    for ($i = 0; $i < $questionNumber; ++$i) {
                                        $question = $questions[$i];
                                        $answersOfQuestion = $question->getAnswers();
                                        $answer = $answers[$j];
                                        if (($answer != NULL)&&($answer->getQuestion() != $question)){
                                            $answer = NULL;
                                        }
                                        else {
                                            $j++;
                                        }
                                        $correct = $question->getCorrectAnswer();
                                        echo "<tr>";
                                        echo "<td>".($i+1)."</td>";
                                        echo "<td>".$question->getText()."</td>"; 
                                        foreach($answersOfQuestion as $answerOfQuestion){
                                            if ($answerOfQuestion == $correct) {
                                                if ($answer == $answerOfQuestion) {
                                                    echo "<td class=\"bg-success\">";
                                                    $points = $points + 3;
                                                }
                                                else {
                                                    echo "<td class=\"bg-warning\">";
                                                }
                                            }
                                            else{
                                                if ($answer == $answerOfQuestion) {
                                                    echo "<td class=\"bg-danger\">";
                                                    $points = $points - 1;
                                                }
                                                else {
                                                    echo "<td>";
                                                }
                                            }
                                            echo $answerOfQuestion->getText()."</td>";
                                        }
                                        echo "<td ".$style."style=\"text-align: center;\"><a href=\"".site_url("Game/reportView/".$question->getId()."")."\"><i class=\"fa fa-flag\"></i></a></td>";                                                
                                        echo "</tr>";
                                    }
                                    if ($this->session->userdata("personId") != NULL){
                                        $score->setPoints($points);
                                        $this->doctrine->em->persist($score);
                                        $this->doctrine->em->flush();
                                    }
                                ?>                                       
                          </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer text-left">
                    <div class="mt-2" style="float:left">
                        Ukupan broj osvojenih poena: <?php echo $points; ?>
                        <?php
                        if ($this->session->userdata("personId") != NULL) {                            
                            if ($session->getPerson() != $person){
                                $owner = $session->getPerson();
                                if ($owner != NULL) {
                                    $username = $owner->getUsername();
                                    $ownerpoints= $session->getScoreForPerson($session->getPerson())->getPoints();
                                    echo "&emsp;(Broj poena koje je imao ".$username." je: ".$ownerpoints.")";
                                }
                            }
                        }
                        ?>
                    </div>
                    <div style="float:right"><a class="btn btn-dark" href="<?php echo site_url('login') ?>">Vrati se na početnu</a></div>
                    <div style="clear:both"></div>
                </div>
            </div>
            <!-- End of content -->

            <!-- Footer -->
            <?php $this->load->view('footer/game'); ?>
            <?php $this->load->view('footer/welcome'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
    </body>
</html>