<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Petar">
        <title>Rang lista - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Page level plugin CSS-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/datatables/css/dataTables.bootstrap4.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php 
            if ($person->isPlayer()){
                $this->load->view('header/player');
            }
            else {
                $this->load->view('header/admin');
            }
        ?>
        <!-- End of header -->
        <div class="content-wrapper">
            <div class="container-fluid">
            <!-- Content -->
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="mt-2" style="float:left"><i class="fa fa-trophy"></i> Rang lista</div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Korisničko ime</th>
                                        <th>Br. poena</th>
                                        <th>Vreme trajanja</th>
                                        <th>Datum</th>
                                        <th>Kategorije</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($scores as $score) {
                                            if (!$score->getIsPlayed()) {
                                                continue;
                                            }
                                            echo "<tr>";
                                            echo "<td>".$score->getPersonUsername()."</td>";
                                            echo "<td>".$score->getPoints()."</td>";
                                            echo "<td>".gmdate("i:s", $score->getTime())."</td>";
                                            echo "<td>".$score->getDate()->format('Y-m-d')."</td>";
                                            echo "<td>";
                                            foreach ($score->getCategories() as $category) {
                                                echo $category->getName()." ";
                                            }
                                            echo "</td></tr>";
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-left">Kremasti kukizi</div>
                </div>
                <!-- End of content -->
            </div>

            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <?php $this->load->view('footer/logout'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?php echo base_url('vendor/datatables/js/jquery.dataTables.js'); ?>"></script>
        <script src="<?php echo base_url('vendor/datatables/js/dataTables.bootstrap4.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/dashboard.js'); ?>"></script>
        <script src="<?php echo base_url('js/game/scoreboard.js'); ?>"></script>
    </body>
</html>

