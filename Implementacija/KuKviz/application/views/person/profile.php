<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Miljan">
        <title>Profil - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Page level plugin CSS-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/datatables/css/dataTables.bootstrap4.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/profile.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php 
        if ($person->isPlayer()){
            $this->load->view('header/player');
        }
        else {
            $this->load->view('header/admin');
        }
        ?>
        <!-- End of header -->
        
        <div class="content-wrapper">
            <!-- Content -->
            <div class="container-fluid">
                <div class="card mb-3">
                    <div class="card-header text-left">
                        <i class="fa fa-user"></i> Moj profil
                    </div>
                    <div class="card-body">
                        <div class="card mb-3">
                            <div class="card-header text-left">
                                <i class="fa fa-info"></i> Informacije
                            </div>
                            <div class="card-body">

                                <?php
                                    if(isset($message)){
                                        echo "<div class=row>";
                                        echo "<div class=\"col mx-3 alert alert-danger text-center\">".$message."</div>"; 
                                        echo "</div>";

                                    }?>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                                         <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th class="text-center">Izmeni</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Ime i prezime:</td>
                                                <td><?php echo $person->getName(); ?></td>
                                                <td class="text-center">
                                                    <a data-toggle="modal" data-target="#nameModal">
                                                        <span class="nav-link-text mr-1"></span>
                                                        <i class="fa fa-edit"></i>
                                                    </a>                       
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Pol:</td>
                                                <td><?php
                                                    if ($person->getGender() == "male"){
                                                        echo "Muški";
                                                    }
                                                    else {
                                                        echo "Ženski";
                                                    }  
                                                ?></td>
                                                <td class="text-center">
                                                    <a data-toggle="modal" data-target="#genderModal">
                                                        <span class="nav-link-text mr-1"></span>
                                                        <i class="fa fa-edit"></i>
                                                    </a>                      
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>E-mail:</td>
                                                <td> <?php echo $person->getEmail(); ?></td>
                                                <td class="text-center">
                                                    <a data-toggle="modal" data-target="#mailModal">
                                                        <span class="nav-link-text mr-1"></span>
                                                        <i class="fa fa-edit"></i>
                                                    </a>                       
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Korisničko ime:</td>
                                                <td> <?php echo $person->getUsername(); ?></td>
                                                <td class="text-center">
                                                    <a data-toggle="modal" data-target="#usernameModal">
                                                        <span class="nav-link-text mr-1"></span>
                                                        <i class="fa fa-edit"></i>
                                                    </a>                       
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Lozinka:</td>
                                                <td><?php
                                                    $pass = "";
                                                    for ($pas = 0; $pas < strlen($person->getPassword()); $pas++){
                                                        $pass=$pass."*";
                                                    }
                                                    echo $pass;
                                                ?></td>
                                                <td class="text-center">
                                                    <a data-toggle="modal" data-target="#passwordModal">
                                                        <span class="nav-link-text mr-1"></span>
                                                        <i class="fa fa-edit"></i>
                                                    </a>                       
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tip:</td>
                                                <td><?php
                                                    if ($person->isPlayer()) {echo "Korisnik";}
                                                    if ($person->isModerator()) {echo "Moderator";}
                                                    if ($person->isAdmin()) {echo "Admin";}
                                                ?></td>
                                                <td class="text-center"> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <div class="card-header text-left">
                                 <i class="fa fa-trophy"></i> Rezultati
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                                <tr>
                                                    <th>Br. poena</th>
                                                    <th>Vreme trajanja</th>
                                                    <th>Datum i vreme</th>
                                                    <th>Kategorije</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($scores as $score) {
                                                        if ((!$score->getIsPlayed())||($score->getPerson() != $person)) {
                                                            continue;
                                                        }
                                                        echo "<tr>";
                                                        echo "<td>".$score->getPoints()."</td>";
                                                        echo "<td>".gmdate("i:s", $score->getTime())."</td>";
                                                        echo "<td>".$score->getDate()->format('Y-m-d G:i:s')."</td>";
                                                        echo "<td>";
                                                        foreach ($score->getCategories() as $category) {
                                                            echo $category->getName()." ";
                                                        }
                                                        echo "</td></tr>";
                                                    }
                                                ?>
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-left">
                        <div style="float:right"><a class="btn btn-dark" href="<?php echo site_url('admin') ?>">Vrati se na početnu</a></div>
                    </div>
                </div>
            </div>
            <!-- End of content -->

            <!-- Footer -->            
            <?php $this->load->view('footer/welcome'); ?>
            <?php $this->load->view('footer/logout'); ?>
            <?php $this->load->view('footer/profile'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?php echo base_url('vendor/datatables/js/jquery.dataTables.js'); ?>"></script>
        <script src="<?php echo base_url('vendor/datatables/js/dataTables.bootstrap4.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/dashboard.js'); ?>"></script>
        <script src="<?php echo base_url('js/person/profile.js'); ?>"></script>
    </body>
</html>

