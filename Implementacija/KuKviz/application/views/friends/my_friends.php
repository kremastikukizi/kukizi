<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Miljan">
        <title>Moji prijatelji - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Page level plugin CSS-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/datatables/css/dataTables.bootstrap4.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/friends.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <div class="content-wrapper">
            <div class="container-fluid d-flex h-100">
                <!-- Header -->
                <?php 
                if ($person->isPlayer()){
                    $this->load->view('header/player');
                }
                else {
                    $this->load->view('header/admin');
                }?>
                <!-- End of header -->

                <!-- Content -->
                <div class="align-self-center w-100">
                    <div class="card mb-3">
                        <div class="card-header text-left"><i class="fa fa-users"></i> Moji prijatelji</div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Ime</th>
                                            <th>Korisničko ime</th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach($users as $user) {
                                                echo "<tr>";
                                                echo "<td>".$user->getName()."</td>";
                                                echo "<td>".$user->getUsername()."</td>";
                                                echo "<td style=\"text-align: center;\"><a href=\"".site_url("Friend/deleteFriend/".$user->getId())."\"><i class=\"fa fa-user-times tebrex\"></i></a></td>";                                                
                                                echo "</tr>";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer small text-muted text-left">Kremasti kukizi</div>
                    </div>
                </div>
                <!-- End of content -->

                <!-- Footer -->
                <?php $this->load->view('footer/welcome'); ?>
                <?php $this->load->view('footer/logout'); ?>
                <!-- End of footer -->
            </div>
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?php echo base_url('vendor/datatables/js/jquery.dataTables.js'); ?>"></script>
        <script src="<?php echo base_url('vendor/datatables/js/dataTables.bootstrap4.js'); ?>"></script>
        <!-- Custom JavaScript for this page -->
        <script src="<?php echo base_url('js/dashboard.js'); ?>"></script>
        <script src="<?php echo base_url('js/game/friends.js'); ?>"></script>
    </body>
</html>
