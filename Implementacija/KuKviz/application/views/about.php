<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Natalija">
        <title>O nama - KuKviz</title>

        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Custom style for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/content-center.css'); ?>">
    </head>
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/welcome'); ?>
        <!-- End of header -->
        
        <div class="container-fluid content-center">
            <!-- Content -->
            <div class="row">
                <div class="col-lg-6 col-sm-12">
                    <div class="card">
                        <div class="card-header text-left"><i class="fa fa-info"></i> O nama</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p>
                                        Ovaj projekat je odrađen od strane tima Kremasti kukizi, kao obavezan projekat na predmetu 
                                        "Principi softverskog inženjerstva", na trećoj godini studija na odseku Softversko inženjerstvo - Elektrotehnički fakultet.
                                    </p>
                                    <p>
                                        Projekat predstavlja sajt kviza. Na sajtu je moguće igrati kao gost ili napraviti nalog.
                                        Kviz se igra tako što se izaberu kategoriju, i proizvoljno pozove prijatelj. Nakon igranja kviza,
                                        ispisuje se rezultat igre. Pozvani prijatelji mogu da prihvate poziv i time odigraju igru sa istim pitanjima.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12">        
                    <div class="card">
                        <div class="card-header text-left"><i class="fa fa-address-book"></i> Kontakt</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="80%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th class="text-center w-50"><i class="fa fa-user"></i> Osoba</th>
                                                    <th class="text-center w-50"><i class="fa fa-address-book"></i> Kontakt</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>KuKviz tim</td>
                                                    <td>kukvizteam@gmail.com</td>
                                                </tr>
                                                <tr>
                                                    <td>Petar Đekanović</td>
                                                    <td>dp150210d@student.etf.bg.ac.rs</td>
                                                </tr>
                                                <tr>
                                                    <td>Aleksa Brkić</td>
                                                    <td>ba150210d@student.etf.bg.ac.rs</td>
                                                </tr>
                                                <tr>
                                                    <td>Miljan Zarubica</td>
                                                    <td>zb150128@student.etf.bg.ac.rs</td>
                                                </tr>
                                                <tr>
                                                    <td>Natalija Yasir</td>
                                                    <td>yn150145@student.etf.bg.ac.rs</td>
                                                </tr>
                                            </tbody>
                                        </table>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of content -->
                    
            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    </body>
</html>