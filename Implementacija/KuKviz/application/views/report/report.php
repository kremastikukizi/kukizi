<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Miljan">
        <title>Prijavljeno pitanje - KuKviz</title>

        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom fonts for this template-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/font-awesome/css/font-awesome.min.css'); ?>">
        <!-- Page level plugin CSS-->
        <link rel="stylesheet" href="<?php echo base_url('vendor/datatables/css/dataTables.bootstrap4.css'); ?>">
        <!-- Custom styles for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
    </head>    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/admin'); ?>
        <!-- End of header -->
        
        <div class="content-wrapper"> 
            <!-- Content -->
            <div class="container-fluid">                
                <form name="form-report" method="POST" action="<?php echo site_url("Report\handleReport\\".$report->getId()) ?>">
                    <div class="card mb-3 mt-5">                        
                        <div class="card-header text-left">
                            <i class="fa fa-cog"></i> Obrada greške
                        </div>                
                        <div class="card-body">
                            <div class="row">
                                <div class="col alert alert-danger text-center mx-3">
                                    Prijavio: <?php echo $report->getPersonUsername(); ?><br>
                                    Tekst prijave: <?php echo $report->getText(); ?>
                                </div>
                            </div><br>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">                            
                                    <tbody>

                                        <?php $question = $report->getQuestion(); ?>

                                        <tr>
                                            <td>Tekst pitanja:</td>
                                            <td><?php echo $question->getText(); ?></td>
                                            <td>
                                                <input type="text" class="form-control" name="newtext" placeholder="Popravi tekst pitanja" size="50">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Prvi odgovor:</td>
                                            <td><?php echo $question->getAnswers()[0]->getText(); ?></td>
                                            <td>
                                                <input type="text" class="form-control" name="newans1" placeholder="Popravi tekst odgovora" size="50">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Drugi odgovor:</td>
                                            <td><?php echo $question->getAnswers()[1]->getText(); ?></td>
                                            <td>
                                                <input type="text" class="form-control" name="newans2" placeholder="Popravi tekst odgovora" size="50">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Treći odgovor:</td>
                                            <td><?php echo $question->getAnswers()[2]->getText(); ?></td>
                                            <td>
                                                <input type="text" class="form-control" name="newans3" placeholder="Popravi tekst odgovora" size="50">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Četvrti odgovor:</td>
                                            <td><?php echo $question->getAnswers()[3]->getText(); ?></td>
                                            <td>
                                                <input type="text" class="form-control" name="newans4" placeholder="Popravi tekst odgovora" size="50">
                                            </td>
                                        </tr>

                                         <tr>
                                            <td>Tačan odgovor:</td>
                                            <td><?php echo $question->getCorrectAnswer()->getText(); ?></td>
                                            <td>
                                                <select class="form-control" name="newcor">
                                                    <?php 
                                                    $answers = $question->getAnswers();
                                                    $options = array();                                
                                                    foreach ($answers as $answer){
                                                        if ($answer->isCorrect() == true){
                                                            $options[] = "selected";
                                                        }
                                                        else {
                                                            $options[] = "";
                                                        }
                                                    }
                                                    ?>
                                                    <option <?php echo $options[0]; ?> >Prvi</option>
                                                    <option <?php echo $options[1]; ?> >Drugi</option>
                                                    <option <?php echo $options[2]; ?> >Treći</option>
                                                    <option <?php echo $options[3]; ?> >Četvrti</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kategorija pitanja:</td>
                                            <td><?php echo $question->getCategoryName(); ?></td>
                                            <td>
                                                <select class="form-control" name="newcat">
                                                    <option selected>Izaberite novu kategoriju</option>
                                                    <?php
                                                        $categories = $this->doctrine->em->getRepository('Entity\Category')->findAll();
                                                        foreach ($categories as $category){
                                                            if ($category->getName() != $question->getCategoryName()){
                                                                echo "<option>".$category->getName()."</option>";
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>                  
                        <div class="card-footer text-left">
                            <div style="float:right"><button class="btn btn-dark" type="submit">Potvrdi</button></div>     
                            <div style="clear:both"></div>
                        </div>
                    </div>            
                </form>
            </div>
            <!-- End of content -->                    
            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <?php $this->load->view('footer/logout'); ?>
            <!-- End of footer -->
        </div>
        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    </body>
</html>