<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Petar">
        <title>Registracija - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom style for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/content-center.css'); ?>">
    </head>

    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/welcome'); ?>
        <!-- End of header -->
            
        <div class="container-fluid content-center">
            <!-- Content -->
            <div class="card card-login mx-auto">
                <div class="card-header">Registracija</div>
                    <div class="card-body">
                        <form name="form-register" method="POST" action="<?php echo site_url('Authentication/register') ?>">
                            <?php if(isset($message)) echo "<div class=\"alert alert-danger\">".$message."</div>";?>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <label for="firstname">Ime</label>
                                        <?php echo form_error("firstname","<div class=\"alert alert-danger\">","</div>"); ?>
                                        <input class="form-control" name="firstname" type="text" placeholder="Unesite Vaše ime" autofocus>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="lastname">Prezime</label>
                                        <?php echo form_error("lastname","<div class=\"alert alert-danger\">","</div>"); ?>
                                        <input class="form-control" name="lastname" type="text" placeholder="Unesite Vaše prezime">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pol">Pol</label>
                                <div class="form-row mt-2">
                                    <div class="col-md-6">
                                        <input name="gender" type="radio" value="male" checked>
                                        <label for="pol">&nbsp;Muški</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input name="gender" type="radio" value="female">
                                        <label for="pol">&nbsp;Ženski</label>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label for="email">Email adresa</label>
                                <?php echo form_error("email","<div class=\"alert alert-danger\">","</div>"); ?>
                                <input class="form-control" name="email" type="email" placeholder="Unesite Vašu e-mail adresu">
                            </div>
                            <div class="form-group">
                                <label for="username">Korisničko ime</label>
                                <?php echo form_error("username","<div class=\"alert alert-danger\">","</div>"); ?>
                                <input class="form-control" name="username" type="text" placeholder="Unesite Vaše korisničko ime">
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <label for="password1">Lozinka</label>
                                        <?php echo form_error("password1","<div class=\"alert alert-danger\">","</div>"); ?>
                                        <input class="form-control" name="password1" type="password" placeholder="Unesite Vašu lozinku">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="password2">Potvrda lozinke</label>
                                        <?php echo form_error("password2","<div class=\"alert alert-danger\">","</div>"); ?>
                                        <input class="form-control" name="password2" type="password" placeholder="Potvrdite Vašu lozinku">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" required>Prihvatam uslove korišćenja</label>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Registrujte se</button>
                        </form>
                        <div class="text-center">
                            <a class="d-block mt-3" href="<?php echo site_url('login') ?>">Prijava korisnika</a>
                            <a class="d-block" href="<?php echo site_url('reset') ?>">Zaboravili ste lozinku?</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of content -->
            
            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    </body>
</html>