<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Petar">
        <title>Prijava - KuKviz</title>

        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom style for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/content-center.css'); ?>">
    </head>
    
    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/welcome'); ?>
        <!-- End of header -->
        
        <div class="container-fluid content-center">
            <!-- Content -->
            <div class="card card-login mx-auto">
                <div class="card-header">Prijava</div>
                <div class="card-body">
                    <form name="form-login" method="POST" action="<?php echo site_url('Authentication/login') ?>">
                        <?php if(isset($message)) echo "<div class=\"alert alert-danger\">".$message."</div>";?>
                        <div class="form-group">
                            <?php echo form_error("username","<div class=\"alert alert-danger\">","</div>"); ?>
                            <label for="username">Korisničko ime</label>
                            <input class="form-control" type="text" name="username" placeholder="Unesite Vaše korisničko ime" value="<?php echo set_value('username') ?>" autofocus>
                        </div>
                        <div class="form-group">
                            <?php echo form_error("password","<div class=\"alert alert-danger\">","</div>"); ?>
                            <label for="password">Lozinka</label>
                            <input class="form-control" type="password" name="password" placeholder="Unesite Vašu lozinku">
                        </div>
                        <button class="btn btn-primary btn-block" type='submit'>Prijavite se</button>
                    </form>
                    <div class="text-center">
                        <a class="d-block mt-3" href="<?php echo site_url('register') ?>">Registracija korisnika</a>
                        <a class="d-block" href="<?php echo site_url('reset') ?>">Zaboravili ste lozinku?</a>
                    </div>
                </div>
            </div>
            <!-- End of content -->
            
            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    </body>
</html>