<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Petar">
        <title>Resetovanje lozinke - KuKviz</title>
        
        <link rel="icon" href="<?php echo base_url('images/icon.png'); ?>">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css'); ?>">
        <!-- Custom style for this page -->
        <link rel="stylesheet" href="<?php echo base_url('css/main.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('css/content-center.css'); ?>">
    </head>

    <body class="fixed-nav sticky-footer">
        <!-- Header -->
        <?php $this->load->view('header/welcome'); ?>
        <!-- End of header -->
            
        <div class="container-fluid content-center">
            <!-- Content -->
            <div class="card card-login mx-auto">
                <div class="card-header">Resetovanje lozinke</div>
                <div class="card-body">
                    <div class="text-center mt-4 mb-5">
                        <h4>Zaboravili ste lozinku?</h4>
                        <p>Unesite Vašu e-mail adresu i poslaćemo Vam e-mail sa novom lozinkom.</p>
                    </div>
                    <form name="form-reset" method="POST" action="<?php echo site_url('Authentication/resetPassword') ?>">
                        <?php if(isset($message)) echo "<div class=\"alert alert-danger\">".$message."</div>";?>
                        <div class="form-group">
                            <?php echo form_error("email","<div class=\"alert alert-danger\">","</div>"); ?>
                            <input class="form-control" name="email" type="email" placeholder="Unesite e-mail adresu">
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Resetujte lozinku</button>
                    </form>
                    <div class="text-center">
                        <a class="d-block mt-3" href="<?php echo site_url('register') ?>">Registracija korisnika</a>
                        <a class="d-block" href="<?php echo site_url('login') ?>">Prijava korisnika</a>
                    </div>
                </div>
            </div>
            <!-- End of content -->
            
            <!-- Footer -->
            <?php $this->load->view('footer/welcome'); ?>
            <!-- End of footer -->
        </div>

        <!-- Jquery core JavaScript -->
        <script src="<?php echo base_url('vendor/jquery/jquery-3.3.1.min.js'); ?>"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    </body>
</html>