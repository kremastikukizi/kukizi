<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Custom routes
$route[''] = 'Welcome';
$route['about'] = 'Welcome/aboutView';

$route['login'] = 'Authentication/loginView';
$route['register'] = 'Authentication/registerView';
$route['reset'] = 'Authentication/resetPasswordView';

$route['admin'] = 'Admin/indexView';
$route['admin/users'] = 'Admin/usersView';
$route['admin/categories'] = 'Admin/categoriesView';
$route['admin/categories/add'] = 'Admin/addCategoryView';
$route['admin/questions'] = 'Admin/questionsView';
$route['admin/questions/add'] = 'Admin/addQuestionsView';

$route['moderator'] = 'Admin/indexView';
$route['moderator/users'] = 'Admin/usersView';
$route['moderator/categories'] = 'Admin/categoriesView';
$route['moderator/categories/add'] = 'Admin/addCategoryView';
$route['moderator/questions'] = 'Admin/questionsView';
$route['moderator/questions/add'] = 'Admin/addQuestionsView';

$route['player'] = 'Player/indexView';

$route['friend/add_friend'] = 'Friend/addFriendView';
$route['friend/my_friends'] = 'Friend/myFriendView';

$route['profile'] = 'Person/profileView';

$route['game'] = 'Game/gameView';
$route['game/choose_category'] = 'Game/chooseCategoryView';
$route['game/end'] = 'Game/end';
$route['scoreboard'] = 'Game/scoreboardView';