<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Kontroliše operacije za admina/moderatora
 *
 * @author Petar
 * 
 * @version 1.0
 */
class Admin extends CI_Controller {
    
    /**
     * @var int $person id admina/moderatora
     */
    private $person;
    
    /**
     * Kreiranje nove instance
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        // Load models
        
        // Check if admin is logged in and redirect if not
        $personId = $this->session->userdata('personId');
        if (isset($personId)) {
            // Redirect to logged person's page
            $person = $this->doctrine->em->find('Entity\Person', $personId);
            if($person->isAdmin()) {
                $this->person = $person;
                return;
            }
            
            if($person->isModerator()) {
                $this->person = $person;
                return;
            }
            else if($person->isPlayer()) {
                redirect("player");
            }
        }
        else {
            redirect("welcome");
        }
    }
    
    /**
     * Podrazumevano prikazuje index strincu
     * 
     * @return void
     */
    public function index() 
    {
        $this->indexView();
    }
    
    /**
     * Prikazuje index stranicu za admina/moderatora
     * 
     * @return void
     */
    public function indexView()
    {
        $this->load->view('admin/index', array('person' => $this->person));
    }
    
    /**
     * Odjavljuje admina/moderatora (i šalje ga na početnu stranu)
     * 
     * @return void
     */
    public function logout()
    {
        $this->session->unset_userdata("personId");
        $this->session->sess_destroy();
        redirect("login");
    }
    
    /**
     * Prikazuje stranicu sa svim korisnicima i operacijama sa njima
     * 
     * @return void
     */
    public function usersView()
    {
        $this->load->view('admin/users/index', array(
            'person' => $this->person,
            'users' => $this->doctrine->em->getRepository('Entity\Person')->findAll()
        ));
    }
    
    /**
     * Prikazuje stranicu sa svim kategorijama i operacijama sa njima
     * 
     * @return void
     */
    public function categoriesView()
    {
        $this->load->view('admin/categories/index', array(
            'person' => $this->person,
            'categories' => $this->doctrine->em->getRepository('Entity\Category')->findAll()
        ));
    }
    
    /**
     * Prikazuje stranicu za dodavanje nove kategorije (i poruku u slučaju greške)
     * 
     * @param string $message poruka u slučaju greške
     * 
     * @return void
     */
    public function addCategoryView($message = NULL)
    {
        $this->load->view('admin/categories/add', array(
            'person' => $this->person,
            'message' => $message
        ));
    }
    
    /**
     * Dodaje novu kategoriju u bazu podataka
     * 
     * @return void
     */
    public function addCategory()
    {
        $this->form_validation->set_rules("name", "Naziv kategorije", "required");
        $this->form_validation->set_message("required","Polje {field} nije popunjeno.");

        if ($this->form_validation->run()) {
            $name = $this->input->post('name');

            // Check category in database
            $em = $this->doctrine->em;
            $qb = $em->createQueryBuilder();

            $qb->select('c')
              ->from('Entity\Category', 'c')
              ->where('c.name like :name')
              ->setParameter('name', $name);
            
            $result = $qb->getQuery()->getOneOrNullResult();
            
            if(!is_null($result)) {
                $this->addCategoryView("Kategorija već postoji!");
            }
            else {
                $newCategory = new Entity\Category($name);
                $em->persist($newCategory);
                $em->flush();
                
                redirect('admin/categories');
            }
        }
        else {
            $this->addCategoryView();
        }
    }
    
    /**
     * Prikazuje stranicu za izmenu kategorije (i poruku u slučaju greške)
     * 
     * @param integer $categoryId id kategorije koja se prikazuje
     * @param string $message poruka u slučaju greške
     * 
     * @return void
     */
    public function editCategoryView($categoryId, $message = NULL)
    {
        $category = $this->doctrine->em->find("Entity\Category", $categoryId);
        $this->load->view('admin/categories/edit', array(
            'category' => $category,
            'person' => $this->person,
            'message' => $message
        ));
    }
    
    /**
     * Menja kategoriju u bazi podataka
     * 
     * @param integer $categoryId id kategorije koja se menja
     * 
     * @return void
     */
    public function editCategory($categoryId)
    {
        $this->form_validation->set_rules("name", "Novi naziv kategorije", "required");
        $this->form_validation->set_message("required","Polje {field} nije popunjeno.");

        if ($this->form_validation->run()) {
            $name = $this->input->post('name');

            $category = $this->doctrine->em->find("Entity\Category", $categoryId);
            
            $otherCategory = $this->doctrine->em->getRepository("Entity\Category")->findOneBy(array('name' => $name));
            
            if ($otherCategory != NULL) {
                $this->editCategoryView($categoryId, "Kategorija već postoji!");
            }
            else {
                $category->setName($name);
                $this->doctrine->em->flush();
                
                redirect('admin/categories');
            }
        }
        else {
            $this->editCategoryView($categoryId);
        }
    }
    
    /**
     * Briše kategoriju iz baza podataka
     * 
     * @param integer $categoryId id kategorije koja se briše
     * 
     * @return void
     */
    public function deleteCategory($categoryId)
    {
        $category = $this->doctrine->em->find("Entity\Category", $categoryId);
        
        $this->doctrine->em->remove($category);
        $this->doctrine->em->flush();
        
        redirect('admin/categories');
    }
    
    /**
     * Prikazuje stranicu sa svim pitanjima i operacijama sa njima
     * 
     * @return void
     */
    public function questionsView()
    {
        $this->load->view('admin/questions/index', array(
            'person' => $this->person,
            'questions' => $this->doctrine->em->getRepository('Entity\Question')->findAll()
        ));
    }
    
    /**
     * Prikazuje stranicu za dodavanje novog pitanja (i prikazuje poruku u slučaju greške)
     * 
     * @param string $message poruka u slučaju greške
     * @param string $message1 poruka u slučaju greške
     * 
     * @return void
     */
    public function addQuestionsView($message = NULL, $message1 = NULL)
    {
        $this->load->view('admin/questions/add', array(
            'person' => $this->person,
            'categories' => $this->doctrine->em->getRepository('Entity\Category')->findAll(),
            'message' => $message,
            'message1' => $message1
        ));
    }
    
    /**
     * Dodaje novo pitanje u bazu podataka
     * 
     * @return void
     */
    public function addQuestion()
    {
        $this->form_validation->set_rules("text", "Tekst pitanja", "required");
        $this->form_validation->set_rules("answer1", "Prvi odgovor", "required");
        $this->form_validation->set_rules("answer2", "Drugi odgovor", "required");
        $this->form_validation->set_rules("answer3", "Treći odgovor", "required");
        $this->form_validation->set_rules("answer4", "Četvrti odgovor", "required");
        $this->form_validation->set_rules("correct", "Tačan odgovor", "required");
        $this->form_validation->set_rules("category", "Kategorija", "required");
        $this->form_validation->set_message("required","Polje {field} nije popunjeno.");
        
        if($this->form_validation->run()){
            $text = $this->input->post('text');
            $answer = array();
            $answer[] = $this->input->post('answer1');
            $answer[] = $this->input->post('answer2');
            $answer[] = $this->input->post('answer3');
            $answer[] = $this->input->post('answer4');
            $correct = $this->input->post('correct');
            $categoryId = $this->input->post('category');
            
            if(($correct == '0') || ($categoryId == '0')) {
                $this->addQuestionsView(NULL, "Izaberite tačan odgovor i kategoriju");
                return;
            }
            
            $em = $this->doctrine->em;
            
            $category = $this->doctrine->em->find('Entity\Category', $categoryId);
            $newQuestion = new \Entity\Question($text, $category, $this->person);
            $em->persist($newQuestion);

            $newAnswers = array();
            for($i = 0; $i < 4; $i++) {
                $newAnswers[] = new \Entity\Answer($answer[$i], ($correct == ($i + 1)), $newQuestion);
                $em->persist($newAnswers[$i]);
            }

            $em->flush();

            redirect('admin/questions');
        }
        else {
            $this->addQuestionsView();
        }
    }  
    
    /**
     * Prikazuje stranicu za izmenu pitanja
     * 
     * @param integer $questionId id pitanja koje se prikazuje
     * 
     * @return void
     */
    public function editQuestionView($questionId)
    {
        $question = $this->doctrine->em->find("Entity\Question", $questionId);
        $this->load->view('admin/questions/edit', array(
            'categories' => $this->doctrine->em->getRepository('Entity\Category')->findAll(),
            'question' => $question,
            'person' => $this->person,
        ));
    }
    
    /**
     * Menja pitanje u bazi podataka
     * 
     * @param integer $questionId id pitanja koje se menja
     * 
     * @return void
     */
    public function editQuestion($questionId)
    {
        $text = $this->input->post('text');
        $answer1 = $this->input->post('answer1');
        $answer2 = $this->input->post('answer2');
        $answer3 = $this->input->post('answer3');
        $answer4 = $this->input->post('answer4');
        $correct = $this->input->post('correct');
        $categoryId = $this->input->post('category');
        
        $question = $this->doctrine->em->find('Entity\Question', $questionId);
        $answers = $question->getAnswers();
        
        for ($i = 0; $i < 4; $i++){
            if ($answers[$i]->isCorrect() == true){
                $answers[$i]->setIsCorrect(false);
            }
            if ($correct == ($i + 1)){
                 $answers[$i]->setIsCorrect(true);
            }
        }        
        if ($text != ""){            
            $question->setText($text);
        }
        if ($answer1 != ""){
            $answers[0]->setText($answer1);
        }
        if ($answer2 != ""){
            $answers[1]->setText($answer2);            
        }
        if ($answer3 != ""){
            $answers[2]->setText($answer3);
        }
        if ($answer4 != ""){
            $answers[3]->setText($answer4);
        }
        if ($categoryId != 0){
            $category = $this->doctrine->em->find("Entity\Category", $categoryId);
            $question->setCategory($category);
        }
        $this->doctrine->em->flush();
        redirect("admin/questions");
    }
    
    /**
     * Briše pitanje iz baze podataka
     * 
     * @param integer $questionId id pitanja koje se briše
     * 
     * @return void
     */
    public function deleteQuestion($questionId){
        $question = $this->doctrine->em->find("Entity\Question", $questionId);

        $this->doctrine->em->remove($question);
        $this->doctrine->em->flush();

        redirect('admin/questions');
    }
    
    /**
     * Prebacuje korisnika u modertora i moderatora u adimna
     * 
     * @param integer $personId id osobe koja se menja
     * 
     * @return void
     */
    public function upgradePerson($personId) {
        $person = $this->doctrine->em->find("Entity\Person", $personId);
        
        $person->upgrade();
        $this->doctrine->em->flush();
        redirect('admin/usersView');
    }
    
    /**
     * Prebacuje moderatora u korisnika
     * 
     * @param integer $personId id osobe koja se menja
     * 
     * @return void
     */
    public function downgradePerson($personId) {
        $person = $this->doctrine->em->find("Entity\Person", $personId);
        
        $person->downgrade();
        $this->doctrine->em->flush();
        redirect('admin/usersView');
    }
    
    /**
     * Briše osobu iz baze podataka
     * 
     * @param integer $personId id osobe koja se menja
     * 
     * @return void
     */
    public function deletePerson($personId) {
        $person = $this->doctrine->em->find("Entity\Person", $personId);
        
        $this->doctrine->em->remove($person);
        $this->doctrine->em->flush();
        redirect('admin/usersView');
    }
}