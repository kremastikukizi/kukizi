<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Kontroliše operacije korisnika
 *
 * @author Petar
 * 
 * @version 1.0
 */
class Player extends CI_Controller {
    
    /**
     * @var int $person id korisnika
     */
    private $person;
    
    /**
     * Kreiranje nove instance
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        // Load models
        
        // Check if player is logged in and redirect if not
        $personId = $this->session->userdata('personId');
        if (isset($personId)) {
            // Redirect to logged person's page
            $person = $this->doctrine->em->find('Entity\Person', $personId);
            if($person->isPlayer()) {
                $this->person = $person;
                return;
            }
            
            if($person->isModerator()) {
                redirect("moderator");
            }
            else if($person->isAdmin()) {
                redirect("admin");
            }
        }
        else {
            redirect("welcome");
        }
    }
    
    /**
     * Podrazumevano prikazuje index stranicu
     * 
     * @return void
     */
    public function index() 
    {
        $this->indexView();
    }
    
    /**
     * Prikazuje index stranicu za korisnika
     * 
     * @return void
     */
    public function indexView()
    {
        $this->load->view('player/index', array('person' => $this->person));
    }
    
    /**
     * Odjavljuje korisnika (i šalje ga na početnu stranu)
     * 
     * @return void
     */
    public function logout()
    {
        $this->session->unset_userdata("personId");
        $this->session->sess_destroy();
        redirect("login");
    }
}
