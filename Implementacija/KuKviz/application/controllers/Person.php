<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Kontroliše operacije admina/moderatora/korisnika
 *
 * @author Miljan
 * 
 * @version 1.0
 */
class Person extends CI_Controller {
    
    /**
     * Kreiranje nove instance
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // Load models        
        
    }
    
    /**
     * Podrazumevano prikazuje stranicu profila
     * 
     * @return void
     */
    public function index() 
    {
        $this->profileView();
    }
    
    /**
     * Prikazuje strincu profila (i poruku u slučaju greške)
     *
     * @param string $message poruka u slučaju greške
     * 
     * @return void
     */
    public function profileView($message = NULL)
    {
        $scores = $this->doctrine->em->getRepository('Entity\Score')->findAll(); 
        $this->load->view('person\profile', array(
            'message' => $message,
            'scores' => $scores,
            'person' => $this->doctrine->em->find('Entity\Person', $this->session->userdata("personId"))
            ));
    }
    
    /**
     * Menja ime korisnika u bazi podataka
     * 
     * @return void
     */
    public function changeName(){
        $person = $this->doctrine->em->find('Entity\Person', $this->session->userdata("personId"));
        $name = $this->input->post("newname");
        if ($name == "") { $this->profileView("Ne možete imati prazno ime!"); }
        else {
            $person->setName($name);
            $this->doctrine->em->persist($person);
            $this->doctrine->em->flush(); 
            $this->profileView();
        }
    }
    
    /**
     * Menja pol korisnika u bazi podataka
     * 
     * @return void
     */
    public function changeGender(){
        $person = $this->doctrine->em->find('Entity\Person', $this->session->userdata("personId"));
        if ($person->getGender() == "male"){
            $person->setGender("female");
        }
        else {
            $person->setGender("male");
        }
        $this->doctrine->em->persist($person);
        $this->doctrine->em->flush(); 
        $this->profileView();
    }    
    
    /**
     * Menja email korisnika u bazi podataka
     * 
     * @return void
     */
    public function changeEmail(){
        $person = $this->doctrine->em->find('Entity\Person', $this->session->userdata("personId"));
        $email = $this->input->post("newmail");
        if ($email == "" ) { $this->profileView("Ne možete imati prazan e-mail!"); }
        else {
            $users = $this->doctrine->em->getRepository('Entity\Person')->findAll();
            $unique = true;
            foreach($users as $user){
                if (($user != $person) && ($user->getEmail() == $email)){
                    $unique = false;
                }
            }
            if ($unique){
                $person->setEmail($email);
                $this->doctrine->em->persist($person);
                $this->doctrine->em->flush(); 
                $this->profileView();
            }
            else { $this->profileView("Neko već koristi taj e-mail!"); }
        }
    }
    
    /**
     * Menja korisničko ime korisnika u bazi podataka
     * 
     * @return void
     */
    public function changeUsername(){
        $person = $this->doctrine->em->find('Entity\Person', $this->session->userdata("personId"));
        $username = $this->input->post("newusername"); 
        if ($username == "" ) { $this->profileView("Ne možete imati prazno korisničko ime!"); }
        else {
            $users = $this->doctrine->em->getRepository('Entity\Person')->findAll();
            $unique = true;
            foreach($users as $user){
                if (($user != $person) && ($user->getUsername() == $username)){
                    $unique = false;
                }
            }
            if ($unique){
                $person->setUsername($username);
                $this->doctrine->em->persist($person);
                $this->doctrine->em->flush(); 
                $this->profileView();
            }
            else {
                $this->profileView("Neko već koristi to korisničko ime!");
            }        
        }
    }
    
    /**
     * Menja lozinku korisnika u bazi podataka
     * 
     * @return void
     */
    public function changePassword(){
        $person = $this->doctrine->em->find('Entity\Person', $this->session->userdata("personId"));
        $password = $this->input->post("newpassword");
        if ($password == "" ) { $this->profileView("Ne možete imati praznu lozinku!"); }
        else {
            $person->setPassword($password);
            $this->doctrine->em->persist($person);
            $this->doctrine->em->flush(); 
            $this->profileView();
        }        
    }
    
    /**
     * Napušta kviz
     * 
     * @return void
     */
    public function exitGame(){
        if ($this->session->userdata("personId") != NULL){
            $person = $this->doctrine->em->find('Entity\Person', $this->session->userdata("personId"));
            $sessionId = $this->session->userdata("session");
            if ($sessionId == NULL) {
                redirect('');
            }
            $session = $this->doctrine->em->find('Entity\Session', $sessionId);
            $score = $session->getScoreForPerson($person);
            if ($score->getIsPlayed() == 0){
                if ($person == $session->getPerson()){
                    $this->doctrine->em->remove($session);
                    $this->doctrine->em->flush();
                }
                else {
                    $this->doctrine->em->remove($score);
                    $this->doctrine->em->flush();
                }
            }
        }
        redirect('');
    }
}
