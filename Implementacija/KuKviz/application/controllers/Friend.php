<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Kontroliše funkcije za prijateljstvo
 *
 * @author Alexa
 * 
 * @version 1.0
 */
class Friend extends CI_Controller {
    
    /**
     * Kreiranje nove instance
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // Load models
        
    }
    
    /**
     * Prikazuje stranicu za dodavanje korisnika za prijatelja
     *
     * @return void
     */
    public function addFriendView(){
        $users = $this->doctrine->em->getRepository('Entity\Person')->findAll();
        
        $personId = $this->session->userdata("personId");
        $person = $this->doctrine->em->find('Entity\Person', $personId);
        
        $friends = $person->getFriends();
        for ($i = 0; $i < count($users); $i++){      
            if ($person->getId() == $users[$i]->getId()){
                array_splice($users, $i, 1);
                $i--;
            }
            else {
                $deleted = false;
                foreach ($friends as $friend){
                    if ($friend->getId() == $users[$i]->getId()){
                        array_splice($users, $i, 1);
                        $i--;
                        $deleted = true;
                        break;
                    }
                }
                if (!$deleted){
                    $hisFriends = $users[$i]->getFriends();
                    foreach ($hisFriends as $hisFriend){
                        if ($hisFriend->getId() == $person->getId()){
                            array_splice($users, $i, 1);
                            $i--;
                            break;
                        }
                    }
                }
            }
        }
        $this->load->view('friends/add_friend', array(
            'person' => $person,
            'users' => $users
        ));
    }
    
    /**
     * Dodaje korisnika za prijatelja u bazi podataka
     *
     * @param integer $user id korisnika sa kojim se stvara prijateljstvo
     * 
     * @return void
     */
    public function addFriend($user){
        $friend = $this->doctrine->em->find('Entity\Person', $user);
        
        $personId = $this->session->userdata("personId");
        $person = $this->doctrine->em->find('Entity\Person', $personId);
        
        $person->getFriends()[] = $friend;
        $this->doctrine->em->flush();
        $this->addFriendView();
    }
    
    /**
     * Prikazuje stranicu sa prijateljima
     *
     * @return void
     */
    public function myFriendView(){
        $personId = $this->session->userdata("personId");
        $person = $this->doctrine->em->find('Entity\Person', $personId);
        $users = array();
        $myFriends = $person->getFriends();
        foreach ($myFriends as $myFriend){
            $hisFriends = $myFriend->getFriends();
            $mutual = false;
            foreach ($hisFriends as $hisFriend){
                if ($hisFriend->getId() == $person->getId()){                   
                    $mutual = true;
                    break;
                }               
            }
            if ($mutual){
                $users[] = $myFriend;
            }
        }
        $this->load->view('friends/my_friends', array('person' => $person,'users' => $users));
    }
    
    /**
     * Briše prijateljstvo iz baze podataka
     *
     * @param integer $user id prijatelja sa kojim se raskida prijateljstvo
     * 
     * @return void
     */
    public function deleteFriend($user){        
        $friend = $this->doctrine->em->find('Entity\Person', $user);        
        $personId = $this->session->userdata("personId");
        $person = $this->doctrine->em->find('Entity\Person', $personId);
        
        $friendsWithMe = $person->getFriendsWithMe();
        for ($i = 0; $i < count($friendsWithMe); $i++){
            if ($friend->getId() == $friendsWithMe[$i]->getId()){
                $friend->getFriends()->removeElement($person);
                $friend->getFriendsWithMe()->removeElement($person);
                $person->getFriends()->removeElement($friend);
                $person->getFriendsWithMe()->removeElement($friend);
                break;
            }
        }
        
        $this->doctrine->em->persist($friend);
        $this->doctrine->em->persist($person);
        $this->doctrine->em->flush();    
        
        $this->myFriendView();
    }
    
    /**
     * Prihvata zahtev za prijateljstvo
     *
     * @param integer $user id prijatelja čiji se zahtev prihvata
     * 
     * @return void
     */
    public function accept($user){
        $friend = $this->doctrine->em->find('Entity\Person', $user);
        
        $personId = $this->session->userdata("personId");
        $person = $this->doctrine->em->find('Entity\Person', $personId);
        
        $person->getFriends()[] = $friend;
        $this->doctrine->em->flush();
        redirect('login');
    }
    
    /**
     * Odbija zahtev za prijateljstvo
     *
     * @param integer $user id prijatelja čiji se zahtev odbija
     * 
     * @return void
     */
    public function decline($user){
        $request = $this->doctrine->em->find('Entity\Person', $user);
        
        $personId = $this->session->userdata("personId");
        $person = $this->doctrine->em->find('Entity\Person', $personId);
        
        $friendsWithMe = $person->getFriendsWithMe();
        for ($i = 0; $i < count($friendsWithMe); $i++){
            if ($request->getId() == $friendsWithMe[$i]->getId()){
                $request->getFriends()->removeElement($person);
                $person->getFriendsWithMe()->removeElement($request);
                break;
            }
        }
        
        $this->doctrine->em->persist($request);
        $this->doctrine->em->persist($person);
        $this->doctrine->em->flush();
        redirect('login');
    }
}
