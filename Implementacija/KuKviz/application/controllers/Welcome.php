<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Kontroliše operacije početne stranice
 *
 * @author Natalija
 * 
 * @version 1.0
 */
class Welcome extends CI_Controller {
    
    /**
     * Kreiranje nove instance
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        // Load models
        
        // Check for already logged person
        $personId = $this->session->userdata('personId');
        if (isset($personId)) {
            // Redirect to logged person's page
            $person = $this->doctrine->em->find('Entity\Person', $personId);
            if($person->isAdmin()) {
                redirect("admin");
            }
            else if($person->isModerator()) {
                redirect("moderator");
            }
            else {
                redirect("player");
            }
        }
    }

    /**
     * Podrazumevano prikazuje početnu stranicu
     * 
     * @return void
     */
    public function index()
    {
        $this->load->view('welcome');
    }
    
    /**
     * Prikazuje stranicu o nama
     * 
     * @return void
     */
    public function aboutView()
    {
        $this->load->view('about');
    }
}