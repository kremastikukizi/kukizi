<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Kontroliše proces autentifikacije
 *
 * @author Petar
 * 
 * @version 1.0
 */
class Authentication extends CI_Controller {
    
    /**
     * Kreiranje nove instance
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        // Load models
        
        // Check for already logged person
        $personId = $this->session->userdata('personId');
        if (isset($personId)) {
            // Redirect to logged person's page
            $person = $this->doctrine->em->find('Entity\Person', $personId);
            if($person->isAdmin()) {
                redirect("admin");
            }
            else if($person->isModerator()) {
                redirect("moderator");
            }
            else {
                redirect("player");
            }
        }
    }
    
    /**
     * Podrazumevano prikazuje strincu za prijavljivanje
     *
     * @return void
     */
    public function index() 
    {
        $this->loginView();
    }
    
    /**
     * Prikazuje stranicu za prijavljivanje (i poruku u slučaju greške)
     * 
     * @param string $message poruka u slučaju greške
     *
     * @return void
     */
    public function loginView($message = NULL)
    {
        $this->load->view('authentication/login', array('message' => $message));
    }
    
    /**
     * Prijavljuje korisnika (ako je forma validna)
     *
     * @return void     * 
     */
    public function login()
    {
        $this->form_validation->set_rules("username", "Korisnicko ime", "required");
        $this->form_validation->set_rules("password", "Lozinka", "required");
        $this->form_validation->set_message("required","Polje {field} nije popunjeno.");
        
        if ($this->form_validation->run()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            
            // Check user in database
            $em = $this->doctrine->em;
            $qb = $em->createQueryBuilder();
            
            $qb->select('p')
               ->from('Entity\Person', 'p')
               ->where('p.username like :username')
               ->andWhere('p.password like :password')
               ->setParameter('username', $username)
               ->setParameter('password', $password);
            
            $result = $qb->getQuery()->getOneOrNullResult();
            
            if(is_null($result)) {
                $this->loginView("Koriničko ime ili lozinka nisu korektni!");
            }
            else {
                // Set session's data
                $this->session->set_userdata('personId', $result->getId());
                
                // Redirect to logged person's page
                if($result->isAdmin()) {
                    redirect("admin");
                }
                else if($result->isModerator()) {
                    redirect("moderator");
                }
                else {
                    redirect("player");
                }
            }
        }
        else {
            $this->loginView();
        }
    }
    
    /**
     * Prikazuje stranicu za registrovanje (i poruku u slučaju greške)
     * 
     * @param string $message poruka u slučaju greške
     *
     * @return void
     */
    public function registerView($message = NULL)
    {
        $this->load->view('authentication/register', array('message' => $message));
    }
    
    /**
     * Registruje korisnika (ako je forma validna)
     *
     * @return void
     */
    public function register()
    {
        $this->form_validation->set_rules("firstname", "Ime", "required");
        $this->form_validation->set_rules("lastname", "Prezime", "required");
        $this->form_validation->set_rules("email", "E-mail", "required");
        $this->form_validation->set_rules("username", "Korisnicko ime", "required");
        $this->form_validation->set_rules("password1", "Lozinka", "required");
        $this->form_validation->set_rules("password2", "Potvrda lozinke", "required");
        $this->form_validation->set_message("required","Polje {field} nije popunjeno.");
        
        if ($this->form_validation->run()) {
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $gender = $this->input->post('gender');
            $email = $this->input->post('email');
            $username = $this->input->post('username');
            $password1 = $this->input->post('password1');
            $password2 = $this->input->post('password2');
            
            // Check passwords
            if($password1 !== $password2) {
                $this->registerView("Lozinke se ne poklapaju!");
            }
            
            // Check username in database
            $em = $this->doctrine->em;
            $qb = $em->createQueryBuilder();
            
            $qb->select('p')
               ->from('Entity\Person', 'p')
               ->where('p.username like :username')
               ->setParameter('username', $username);
            
            $result = $qb->getQuery()->getOneOrNullResult();
            
            if(!is_null($result)) {
                $this->registerView("Korisničko ime već zauzeto!");
                return;
            }
            
            // Check email in database
            $qb = $em->createQueryBuilder();
            
            $qb->select('p')
               ->from('Entity\Person', 'p')
               ->where('p.email like :email')
               ->setParameter('email', $email);
            
            $result = $qb->getQuery()->getOneOrNullResult();
            
            
            if(!is_null($result)) {
                $this->registerView("Email adresa je već zauzeta!");
            }
            else {
                // Create user and save it in database
                $newPerson = new Entity\Person(
                        $firstname." ".$lastname,
                        $email,
                        $username,
                        $password1,
                        $gender
                        );
                $em->persist($newPerson);
                $em->flush();
                
                redirect('login');
            }
        }
        else {
            $this->registerView();
        }
    }
    
    /**
     * Prikazuje stranicu za resetovanje lozinke (i poruku u slučaju greške)
     * 
     * @param string $message poruka u slučaju greške
     *
     * @return void
     */
    public function resetPasswordView($message = NULL)
    {
        $this->load->view('authentication/reset-password', array('message' => $message));
    }
    
    /**
     * Generiše nasumičnu lozinku
     * 
     * @param string $length dužina generisane lozinke
     *
     * @return string generisna lozinka
     */
    public function random_password($length = 10) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = "";
        for ($i = 0; $i < $length; $i++) {
            $password .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        
        return $password;
    }
    
    /**
     * Resetuje lozinku korisniku (ako postoji)
     *
     * @return void
     */
    public function resetPassword()
    {
        $this->form_validation->set_rules("email", "E-mail", "required");
        $this->form_validation->set_message("required","Polje {field} nije popunjeno.");
        if ($this->form_validation->run()) {
            $email = $this->input->post('email');
            
            // Check if user with this email exists
            $em = $this->doctrine->em;
            $qb = $em->createQueryBuilder();
            
            $qb->select('p')
               ->from('Entity\Person', 'p')
               ->where('p.email like :email')
               ->setParameter('email', $email);
            
            $result = $qb->getQuery()->getOneOrNullResult();
            
            if(is_null($result)) {
                $this->resetPasswordView("Ne postoji korisnik sa unesenom e-mail adresom!");
            }
            else {
                // Generate new password
                $password = $this->random_password();

                // Send email to user with new password
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'kukvizteam@gmail.com',
                    'smtp_pass' => 'kukvizkukviz',
                    'mailtype' => 'text',
                    'charset' => 'utf8_unicode_ci',
                    'wordwrap' => TRUE
                    );
                $this->load->library('email', $config);
                
                $this->email->set_newline("\r\n");
                $this->email->from("kukvizteam@gmail.com", "KuKviz Tim");
                $this->email->to($email);
                $this->email->cc("miljanzarubica1@gmail.com");
                
                $this->email->subject("Resetovanje lozinke");
                $this->email->message(
                        "Poštovani korisniče, \r\n\r\n".
                        "U nastavku se nalazi Vaša nova lozinka. \r\n\r\n".
                        "Korisničko ime: ".$result->getUsername()."\r\n".
                        "Lozinka: ".$password."\r\n\r\n".
                        "Kremasti pozdrav, \r\n".
                        "KuKviz tim"
                    );
                
                if($this->email->send()) {
                    // Set password only if email is send
                    $result->setPassword($password);
                    $em->flush();
                    
                    redirect('login');
                }
                else {
                    $this->resetPasswordView("E-mail nije uspešno poslat!");
                }
            }
        }
        else {
            $this->resetPasswordView();
        }
    }
}
