<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Kontroliše operacije prijave pitanja
 *
 * @author Miljan
 * 
 * @version 1.0
 */
class Report extends CI_Controller {
    
    /**
     * Kreiranje nove instance
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // Load models        
        
    }
    
    /**
     * Podrazumevano prikazuje stranicu za rešvanje prijavljenog pitanja
     *
     * @param integer $report id prijave
     * 
     * @return void
     */
    public function index($report) 
    {
        $this->reportView($report);
    }
    
    /**
     * Prikazuje stranicu za rešvanje prijavljenog pitanja (i poruku u slučaju greške)
     *
     * @param integer $report id prijave
     * @param string $message poruka u slučaju greške
     * 
     * @return void
     */
    public function reportView($report, $message = NULL)
    {
        $r =  $this->doctrine->em->find('Entity\Report', $report);  
        $this->load->view('report\report', array(
            'report' => $r,
            'person' => $this->doctrine->em->find('Entity\Person', $this->session->userdata("personId")) ));
    }
    
    
    /**
     * Menja pitanje u bazi podataka
     *
     * @param integer $report id prijave
     * 
     * @return void
     */
    public function handleReport($report)
    {
        $r =  $this->doctrine->em->find('Entity\Report', $report);
        $question = $r->getQuestion();
        $answers = $question->getAnswers();
        $newText = $this->input->post("newtext");
        $newAns1 = $this->input->post("newans1");
        $newAns2 = $this->input->post("newans2");
        $newAns3 = $this->input->post("newans3");
        $newAns4 = $this->input->post("newans4");
        $newCor = $this->input->post("newcor");
        $newCat = $this->input->post("newcat");
        $options = array("Prvi", "Drugi", "Treći", "Četvrti");
        for ($i = 0; $i < 4; $i++){
            if ($answers[$i]->isCorrect() == true){
                $answers[$i]->setIsCorrect(false);
            }
            if ($newCor == $options[$i]){
                 $answers[$i]->setIsCorrect(true);
            }
        }        
        if ($newText != ""){            
            $question->setText($newText);
        }
        if ($newAns1 != ""){
            $answers[0]->setText($newAns1);
        }
        if ($newAns2 != ""){
            $answers[1]->setText($newAns2);            
        }
        if ($newAns3 != ""){
            $answers[2]->setText($newAns3);            
        }
        if ($newAns4 != ""){
            $answers[3]->setText($newAns4);            
        }        
        if ($newCat != "Izaberite novu kategoriju"){ 
            $categories = $this->doctrine->em->getRepository('Entity\Category')->findAll();
            foreach ($categories as $category){
                if ($category->getName() == $newCat){
                    $question->setCategory($category);
                    break;
                }
            }
        }
        $r->setIsHandled(true);
        $this->doctrine->em->flush();        
        redirect("login");
    }
}
