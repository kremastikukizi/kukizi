<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Kontroliše proces igranja igre
 *
 * @author Miljan
 * 
 * @version 1.0
 */
class Game extends CI_Controller {
    
    /**
     * Kreiranje nove instance
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // Load models        
        
    }
    
    /**
     * Podrazumevano prikazuje strincu za biranje kategorija i pozivanje prijatelja na igru
     *
     * @return void
     */
    public function index() 
    {
        redirect('Game/chooseCategoryView');
    }
    
    /**
     * Prikazuje strincu za biranje kategorija i pozivanje prijatelja na igru (i poruku u slučaju greške)
     *
     * @param string $message poruka u slučaju greške
     * 
     * @return void
     */
    public function chooseCategoryView($message = NULL)
    {   
        $categories = $this->doctrine->em->getRepository('Entity\Category')->findAll();
        $validCategories = array();
        foreach($categories as $category){
            if (count($category->getQuestions()) >= 30){
                $validCategories[] = $category;
            }
        }
        $person = NULL;
        if ($this->session->userdata('personId') != NULL){
            $person = $this->doctrine->em->find('Entity\Person', $this->session->userdata('personId'));
        }
        $this->load->view('game/choose_category', array(
            'person' => $person,
            'message' => $message,
            'categories' => $validCategories
            ));
    }
    
    /**
     * Prikazuje stranicu za igranje kviza
     *
     * @return void
     */
    public function gameView()
    {
        $session = $this->doctrine->em->find('Entity\Session', $this->session->userdata('session'));
        $questions = $session->getQuestions();
        $this->load->view('game/index', array(    
            'person' => $this->doctrine->em->find('Entity\Person', $this->session->userdata('personId')),
            'questions' => $questions
        ));
    }
    
    /**
     * Nalazi pitanja koja će biti na kvizu (i upisuje ih u bazu podataka) i poziva prijatelje
     *
     * @return void
     */
    public function chooseCategory()
    {        
        $categories = $this->doctrine->em->getRepository('Entity\Category')->findAll();
        $allquestions = array();
        $quizlength = 0;
        $catnum = 0;
        if ($this->input->post('length') === "Kratka") {
            $quizlength = 10;
        }
        else if ($this->input->post('length') === "Srednja"){
            $quizlength = 20;
        }
        else {
            $quizlength = 30;
        }
        foreach ($categories as $category){
            if ($this->input->post("catcb".$category->getId()) != ""){
                $catnum++;
            }
        }
        $catquenum = 0;
        if ($catnum != 0) { $catquenum = ceil($quizlength / $catnum); }
        foreach ($categories as $category){
            if ($this->input->post("catcb".$category->getId()) != ""){
                $questions = $category->getQuestions()->toArray();
                shuffle($questions);
                for ($i = 0; $i < $catquenum; $i++){
                    $allquestions[] = $questions[$i];
                }
            }
        }
        shuffle($allquestions);
        array_splice($allquestions, $quizlength);
        
        
        if (count($allquestions) == 0){ 
            $this->chooseCategoryView("Morate izabrati barem jednu kategoriju");
        }
        else { 
            if ($this->session->userdata('personId') != NULL) {
                $person = $this->doctrine->em->find('Entity\Person', $this->session->userdata('personId'));            
                $session = new Entity\Session($this->doctrine->em->find('Entity\Person',
                        $this->session->userdata('personId')));
                $this->doctrine->em->persist($session);
                $this->doctrine->em->flush();
                $session->addQuestions($allquestions);
                $this->doctrine->em->persist($session);
                $this->doctrine->em->flush();
                $this->session->set_userdata('session', $session->getId());

                $friends = $person->getFriends();

                $score = new Entity\Score($person, $session, 0, Game::getToday(), 0, 0);
                $this->doctrine->em->persist($score);

                foreach ($friends as $friend) {
                    if ($this->input->post("friendcb".$friend->getId()) != "") {
                        $score = new Entity\Score($friend, $session, 0, Game::getToday(), 0, 0);
                        $this->doctrine->em->persist($score);
                    }
                }
                $this->doctrine->em->flush();

                redirect("game");
            }
            else {
                $questionsId = array();
                foreach ($allquestions as $question){
                    $questionsId[] = $question->getId();
                }
                $this->session->set_userdata('guestquestions', $questionsId);
                $this->load->view('game/index', array('questions' => $allquestions));                
            }
        }        
    }
    
    /**
     * Stvara skor za osobu i upisuje njegove odgovore u bazu podataka
     *
     * @return void
     */
    public function end(){
        if ($this->session->userdata('personId') != NULL){
            $session = $this->doctrine->em->find('Entity\Session', $this->session->userdata('session'));
            $questions = $session->getQuestions();
            $num = count($questions);
            $person =  $this->doctrine->em->find('Entity\Person', $this->session->userdata('personId'));

            $answers = array();
            for ($i = 1; $i < $num + 1; $i++){
                if ($this->input->post($i."answered") == 5) {
                    $answers[$i-1] = NULL;
                }
                else {
                    $answers[$i-1] = $questions[$i-1]->getAnswers()[$this->input->post($i."answered") - 1];
                }
            }

            $score = $session->getScoreForPerson($person);
            $score->setDate(Game::getToday());
            $score->setTime(intval($this->input->post("sec")));
            $score->setIsPlayed(1);
            $score->addAnswers($answers);
            $this->doctrine->em->persist($score);
            $this->doctrine->em->flush();

            $this->load->view('game/end', array(
                'person' => $person,
                'session' => $session               
            ));
        }
        else { 
            $questionsId = $this->session->userdata('guestquestions');
            $questions = array();
            foreach ($questionsId as $questionId){
                $questions[] = $this->doctrine->em->find('Entity\Question', $questionId);
            }
            $num = count($questions);
            
            $answers = array();
            for ($i = 1; $i < $num + 1; $i++){
                if ($this->input->post($i."answered") == 5) {
                    $answers[$i-1] = NULL;
                }
                else {
                    $answers[$i-1] = $questions[$i-1]->getAnswers()[$this->input->post($i."answered") - 1];
                }
            }
            $this->load->view('game/end', array(
                'questions' => $questions,
                'answers' => $answers
            ));
        }
    }
    
    
    /**
     * Prikazuje stranicu za prijavu greške
     * 
     * @param integer $questionId id pitanja za koje se greška prijavljuje
     *
     * @return void
     */
    public function reportView($questionId){
        $this->load->view("game/report", array(
            'person' => $this->doctrine->em->find('Entity\Person', $this->session->userdata('personId')),
            'question' => $questionId));
    }
    
    /**
     * Ubacuje prijavu u bazu podataka
     * 
     * @param integer $questionId id pitanja za koje se greška prijavljuje
     *
     * @return void
     */
    public function report($questionId) {
        $text = $this->input->post("reportText");
        
        $question = $this->doctrine->em->find("Entity\Question", $questionId);
        
        $personId = $this->session->userdata("personId");
        $person = $this->doctrine->em->find("Entity\Person", $personId);
        
        $sessionId = $this->session->userdata("session");
        $session = $this->doctrine->em->find("Entity\Session", $sessionId);
        
        $report = new Entity\Report($text, $question, $person);
        $this->doctrine->em->persist($report);
        $this->doctrine->em->flush();
        
        $this->load->view("game/end", array(
            'person' => $person,
            'session' => $session
            ));
    }
    
    /**
     * Prikazuje stranicu sa rang listom
     *
     * @return void
     */
    public function scoreboardView() {
        $qb = $this->doctrine->em->createQueryBuilder();
        $qb->select("s")
                ->from("Entity\Score", "s")
                ->orderBy("s.points", "DESC");
        $scores = $qb->getQuery()->getResult();
        
        $this->load->view("game/scoreboard.php", array(
            'person' => $this->doctrine->em->find('Entity\Person', $this->session->userdata('personId')),
            "scores" => $scores));
    }
    
    /**
     * Prihvata zahtev za igru i pokreće je
     * 
     * @param integer $scoreId id zahteva koji se prihvata
     *
     * @return void
     */
    public function acceptInvite($scoreId) {
        $score = $this->doctrine->em->find("Entity\Score", $scoreId);
        $session = $score->getSession();
        
        $this->session->set_userdata('session', $session->getId());
        redirect('game');
    }
    
    /**
     * Odbija zahtev za igru
     * 
     * @param integer $scoreId id zahteva koji se odbija
     *
     * @return void
     */
    public function declineInvite($scoreId) {
        $score = $this->doctrine->em->find("Entity\Score", $scoreId);
        
        $this->doctrine->em->remove($score);
        $this->doctrine->em->flush();
        
        redirect('login');
    }
    
    /**
     * Dohvata trenutno vreme
     *
     * @return date
     */
    protected static function getToday() {
        date_default_timezone_set("Europe/Belgrade");
        $date_string = date('d-m-Y');
        $time = date_create_from_format('d-m-Y', "$date_string");
        
        return $time;
    }
}
