<?php

namespace Entity;

/**
 * Description of Person
 * General class for all users. A user can be a player or a moderator (a moderator can also
 * be an admin).
 *
 * @author Alexa
 * @Entity
 * @Table (name="person")
 */
class Person {
    /**
     * @Id
     * @Column (name="idperson", type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;
    
    /**
     * @Column (name="name", type="string", length=45, nullable=false)
     */
    protected $name;
    
    /**
     * @Column (name="email", type="string", length=45, nullable=false)
     */
    protected $email;
    
    /**
     * @Column (name="username", type="string", length=45, nullable=false)
     */
    protected $username;
    
    /**
     * @Column (name="password", type="string", length=45, nullable=false)
     */
    protected $password;
    
    /**
     * @Column (name="gender", type="string", length=10, nullable=false)
     */
    protected $gender;
    
    /**
     * Player, Moderator, or Admin
     * @Column (name="type", type="string", length=10, nullable=false)
     */
    protected $type;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Relations
     * 
     * -------------------------------------------------------------------------
     */
    
    /**
     * @ManyToMany (targetEntity="person", inversedBy="friendsWithMe")
     * @JoinTable (name="friends",
     *      joinColumns={@JoinColumn(name="idperson", referencedColumnName="idperson", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="idfriend", referencedColumnName="idperson", onDelete="CASCADE")}
     * )
     */
    protected $friends;
    
    /**
     * @ManyToMany (targetEntity="person", mappedBy="friends")
     */
    protected $friendsWithMe;
    
    /**
     * Scores from the games that the person has played
     * 
     * @OneToMany (targetEntity="score", mappedBy="person")
     */
    protected $scores;
    
    /**
     * Mistake reports that the person has reported
     * 
     * @OneToMany (targetEntity="report", mappedBy="person")
     */
    protected $reports;
    
    /**
     * --- For moderator ---
     * The questions that the moderator has submitted
     * 
     * @OneToMany (targetEntity="question", mappedBy="moderator")
     */
    protected $questions;
    
    /**
     * The session that were started by this person.
     * 
     * @OneToMany (targetEntity="session", mappedBy="person")
     */
    protected $sessions;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Methods
     * 
     * -------------------------------------------------------------------------
     */
    
    public function __construct($name, $email, $username, $password, $gender, $type = null) {
        if (($gender != "male") && ($gender != "female")) {
            $gender = "female";
        }
        if (($type != "player") && ($type != "moderator") && ($type != "admin")) {
            $type = "player";
        }
        $this->id = 0;
        $this->name = $name;
        $this->email = $email;
        $this->username = $username;
        $this->password = $password;
        $this->gender = $gender;
        $this->type = $type;
    }
    //--------------------------------------------------------------------------
    public function getId() {
        return $this->id;
    }
    //--------------------------------------------------------------------------
    public function getName() {
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    //--------------------------------------------------------------------------
    public function getEmail() {
        return $this->email;
    }
    public function setEmail($email) {
        $this->email = $email;
    }
    //--------------------------------------------------------------------------
    public function getUsername() {
        return $this->username;
    }
    public function setUsername($username) {
        $this->username = $username;
    }
    //--------------------------------------------------------------------------
    public function getPassword() {
        return $this->password;
    }
    public function setPassword($password) {
        $this->password = $password;
    }
    //--------------------------------------------------------------------------
    public function getGender() {
        return $this->gender;
    }
    public function setGender($gender) {
        if ($gender == "male" || $gender == "female") {
            $this->gender = $gender;
        }
    }
    //--------------------------------------------------------------------------
    public function getFriends() {
        return $this->friends;
    }
    public function addFriend($friend) {
        $this->friends->add($friend);
    }
    //--------------------------------------------------------------------------
    public function getFriendsWithMe() {
        return $this->friendsWithMe;
    }
    public function addFriendWithMe($friend) {
        $friend->getFriendsWithMe()->add($this);
    }
    //--------------------------------------------------------------------------
    public function getScores() {
        return $this->scores;
    }
    public function addScore($score) {
        $score->setPerson($this);
    }
    public function getUnplayedScores() {
        $ret = array();
        foreach ($this->scores as $score) {
            if (!$score->getIsPlayed()) {
                $ret[] = $score;
            }
        }
        return $ret;
    }
    //--------------------------------------------------------------------------
    public function getReports() {
        return $this->reports;
    }
    public function addReport($report) {
        $report->setPerson($this);
    }
    //--------------------------------------------------------------------------
    public function getQuestions() {
        if ($this->isPlayer()) {
            return null;
        }
        return $this->questions;
    }
    public function addQuestion($question) {
        if ($this->isPlayer()) {
            return;
        }
        $question->setPerson($this);
    }
    //--------------------------------------------------------------------------
    public function getSessions() {
        return $this->sessions;
    }
    public function addSession($session) {
        $session->setPerson($this);
    }
    //--------------------------------------------------------------------------
    public function isPlayer() {
        return $this->type == "player";
    }
    public function isModerator() {
        return $this->type == "moderator";
    }
    public function isAdmin() {
        return $this->type == "admin";
    }
    //--------------------------------------------------------------------------
    public function turnIntoPlayer() {
        $this->type = "player";
    }
    public function turnIntoModerator() {
        $this->type = "moderator";
    }
    public function turnIntoAdmin() {
        $this->type = "admin";
    }
    //--------------------------------------------------------------------------
    public function getType() {
        return $this->type;
    }
    //--------------------------------------------------------------------------
    public function upgrade() {
        if ($this->isModerator()) {
            $this->turnIntoAdmin();
        }
        if ($this->isPlayer()) {
            $this->turnIntoModerator();
        }
    }
    public function downgrade() {
        if ($this->isModerator()) {
            $this->turnIntoPlayer();
        }
    }
    public function typeToString() {
        if($this->isAdmin()) {
            return "Admin";
        }
        else if($this->isModerator()) {
            return "Modertor";
        }
        else {
            return "Korisnik";
        }
    }
}
