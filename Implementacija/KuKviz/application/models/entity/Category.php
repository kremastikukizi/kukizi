<?php

namespace Entity;

/**
 * Description of category
 * A category of questions.
 *
 * @author Alexa
 * @Entity
 * @Table (name="category")
 */
class Category {
    /**
     * @Id
     * @Column (name="idcategory", type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;
    
    /**
     * @Column (name="name", type="string", nullable=false, length=45)
     */
    protected $name;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Relations
     * 
     * -------------------------------------------------------------------------
     */
    
    /**
     * Questions that belong to this category
     * 
     * @OneToMany (targetEntity="question", mappedBy="category")
     */
    protected $questions;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Methods
     * 
     * -------------------------------------------------------------------------
     */
    
    public function __construct($name) {
        $this->id = 0;
        $this->name = $name;
    }
    //--------------------------------------------------------------------------
    public function getId() {
        return $this->id;
    }
    //--------------------------------------------------------------------------
    public function getName() {
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    //--------------------------------------------------------------------------
    public function getQuestions() {
        return $this->questions;
    }
    public function addQuestion($question) {
        $question->setCategory($this);
    }
}
