<?php

namespace Entity;

/**
 * Description of Question
 * A question for the quiz. New questions can be submitted by moderators. A question contains
 * multiple answers, with only one correct answer.
 *
 * @author Alexa
 * @Entity
 * @Table (name="question")
 */
class Question {
    /**
     * @Id
     * @Column (name="idquestion", type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;
    
    /**
     * @Column (name="text", type="text")
     */
    protected $text;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Relations
     * 
     * -------------------------------------------------------------------------
     */
    
    /**
     * The category that the question belongs to
     * 
     * @ManyToOne (targetEntity="category", inversedBy="questions")
     * @JoinColumn (name="idcategory", referencedColumnName="idcategory", nullable=false, onDelete="CASCADE")
     */
    protected $category;
    
    /**
     * All answers (correct and incorrect) for the question
     * 
     * @OneToMany (targetEntity="answer", mappedBy="question")
     */
    protected $answers;
    
    /**
     * The moderator that created and submitted the question
     * 
     * @ManyToOne (targetEntity="person", inversedBy="questions")
     * @JoinColumn (name="idperson", referencedColumnName="idperson", onDelete="SET NULL")
     */
    protected $moderator;
    
    /**
     * Mistake reports that have been reported for this question
     * 
     * @OneToMany (targetEntity="report", mappedBy="question")
     */
    protected $reports;
    
    /**
     * Sessions that contain this question
     * 
     * @ManyToMany (targetEntity="session", mappedBy="questions")
     */
    protected $sessions;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Methods
     * 
     * -------------------------------------------------------------------------
     */
    
    public function __construct($text, $category, $moderator) {
        $this->id = 0;
        $this->text = $text;
        $this->category = $category;
        $this->moderator = $moderator;
    }
    //--------------------------------------------------------------------------
    public function getId() {
        return $this->id;
    }
    //--------------------------------------------------------------------------
    public function getText() {
        return $this->text;
    }
    public function setText($text) {
        $this->text = $text;
    }
    //--------------------------------------------------------------------------
    public function getCategory() {
        return $this->category;
    }
    public function setCategory($category) {
        $this->category = $category;
    }
    public function getCategoryName() {
        return $this->category->getName();
    }
    //--------------------------------------------------------------------------
    public function getModerator() {
        return $this->moderator;
    }
    public function setModerator($moderator) {
        $this->moderator = $moderator;
    }
    public function getModeratorName() {
        return $this->moderator->getName();
    }
    public function getModeratorUsername() {
        return $this->moderator->getUsername();
    }
    public function getModeratorEmail() {
        return $this->moderator->getEmail();
    }
    public function isModerator() {
        return $this->moderator->isModerator();
    }
    public function isAdmin() {
        return $this->moderator->isAdmin();
    }
    //--------------------------------------------------------------------------
    public function getAnswers() {
        return $this->answers;
    }
    public function addAnswer($answer) {
        $answer->setQuestion($this);
    }
    public function getCorrectAnswer() {
        $answers = $this->answers;
        foreach($answers as $answer) {
            if ($answer->isCorrect()) {
                return $answer;
            }
        }
    }
    //--------------------------------------------------------------------------
    public function getReports() {
        return $this->reports;
    }
    public function addReport($report) {
        $report->setQuestion($this);
    }
}
