<?php

namespace Entity;

/**
 * Description of Answer
 * One of possible answers for a question.
 *
 * @author Alexa
 * @Entity
 * @Table (name="answer")
 */
class Answer {
    /**
     * @Id
     * @Column (name="idanswer", type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;
    
    /**
     * @Column (name="text", type="text", nullable=false)
     */
    protected $text;
    
    /**
     * Whether the answer is correct or not
     * 
     * @Column (name="iscorrect", type="boolean", nullable=false)
     */
    protected $isCorrect = false;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Relations
     * 
     * -------------------------------------------------------------------------
     */
    
    /**
     * The question that the answer is for
     * 
     * @ManyToOne (targetEntity="question", inversedBy="answers")
     * @JoinColumn (name="idquestion", referencedColumnName="idquestion", nullable=false, onDelete="CASCADE")
     */
    protected $question;
    
    /**
     * The question that the answer is for
     * 
     * @ManyToMany (targetEntity="score", mappedBy="answers")
     */
    protected $scores;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Methods
     * 
     * -------------------------------------------------------------------------
     */
    
    public function __construct($text, $isCorrect, $question) {
        $this->id = 0;
        $this->text = $text;
        $this->isCorrect = $isCorrect;
        $this->question = $question;
    }
    //--------------------------------------------------------------------------
    public function getId() {
        return $this->id;
    }
    //--------------------------------------------------------------------------
    public function getText() {
        return $this->text;
    }
    public function setText($text) {
        $this->text = $text;
    }
    //--------------------------------------------------------------------------
    public function isCorrect() {
        return $this->isCorrect;
    }
    public function setIsCorrect($isCorrect) {
        $this->isCorrect = $isCorrect;
    }
    //--------------------------------------------------------------------------
    public function getQuestion() {
        return $this->question;
    }
    public function setQuestion($question) {
        $this->question = $question;
    }
    public function getQuestionText() {
        return $this->question->getText();
    }
    public function getCorrectAnswer() {
        return $this->question->getCorrectAnswer();
    }
    //--------------------------------------------------------------------------
    public function getScores() {
        return $this->scores;
    }
    public function addScore($score) {
        $score->getAnswers()->add($this);
    }
}
