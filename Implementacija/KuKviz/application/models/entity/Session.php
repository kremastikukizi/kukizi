<?php

namespace Entity;

/**
 * Set of questions from a game.
 * Multiple scores are connected to a session, because a person can invite
 * his friends to play the same session.
 *
 * @author Alexa
 * @Entity
 * @Table (name="session")
 */
class Session {
    /**
     * @Id
     * @Column (name="idsession", type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Relations
     * 
     * -------------------------------------------------------------------------
     */
    
    /**
     * Questions belonging to the session.
     * 
     * @ManyToMany (targetEntity="question", inversedBy="sessions")
     * @JoinTable (name="session_question",
     *      joinColumns={@JoinColumn(name="idsession", referencedColumnName="idsession", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="idquestion", referencedColumnName="idquestion", onDelete="CASCADE")}
     * )
     */
    protected $questions;
    
    /**
     * The person who originally started the session.
     * 
     * @ManyToOne (targetEntity="person", inversedBy="sessions")
     * @JoinColumn (name="idperson", referencedColumnName="idperson", onDelete="SET NULL")
     */
    protected $person;
    
    /**
     * Scores connected to the session.
     * 
     * @OneToMany (targetEntity="score", mappedBy="session")
     */
    protected $scores;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Methods
     * 
     * -------------------------------------------------------------------------
     */
    
    public function __construct($person) {
        $this->id = 0;
        $this->person = $person;  
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    //--------------------------------------------------------------------------
    public function getId() {
        return $this->id;
    }
    //--------------------------------------------------------------------------
    public function getQuestions() {
        return $this->questions;
    }
    public function addQuestion($question) {
        $this->questions->add($question);
    }
    public function addQuestions($questions){
        foreach($questions as $question){
            $this->questions->add($question);
        }
    }
    //--------------------------------------------------------------------------
    public function getPerson() {
        return $this->person;
    }
    public function setPerson($person) {
        $this->person = $person;
    }
    public function getPersonName() {
        if ($this->person == NULL) {
            return "nepoznati korisnik";
        }
        return $this->person->getName();
    }
    public function getPersonUsername() {
        if ($this->person == NULL) {
            return "nepoznati korisnik";
        }
        return $this->person->getUsername();
    }
    public function getPersonEmail() {
        if ($this->person == NULL) {
            return "nepoznati korisnik";
        }
        return $this->person->getEmail();
    }
    //--------------------------------------------------------------------------
    public function getScores() {
        return $this->scores;
    }
    public function addScore($score) {
        $score->setSession($this);
    }
    //--------------------------------------------------------------------------
    public function getCategories() {
        $ret = array();
        
        foreach ($this->questions as $question) {
            if (!in_array($question->getCategory(), $ret)) {
                $ret[] = $question->getCategory();
            }
        }
        return $ret;
    }
    //--------------------------------------------------------------------------
    public function getCorrectAnswers() {
        $ret = array();
        
        foreach ($this->questions as $question) {
            $ret[] = $question->getCorrectAnswer();
        }
        return ret;
    }
    //--------------------------------------------------------------------------
    public function getScoreForPerson($person){
        foreach($this->scores as $score){
            if ($score->getPerson() == $person){
                return $score;
            }           
        }
        return NULL;
    }
}
