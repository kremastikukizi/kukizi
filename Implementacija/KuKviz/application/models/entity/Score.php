<?php

namespace Entity;

/**
 * Description of Score
 * Score from a single game.
 *
 * @author Alexa
 * @Entity
 * @Table (name="score")
 */
class Score {
    /**
     * @Id
     * @Column (name="idscore", type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;
    
    /**
     * If the player is invited, his game isn't played.
     * 
     * @Column (name="isPlayed", type="boolean", nullable=false)
     */
    protected $isPlayed;
    
    /**
     * Number of points scored in the game
     * 
     * @Column (name="points", type="integer", nullable=false)
     */
    protected $points;
    
    /**
     * Time at which the game was played
     * 
     * @Column (name="date", type="datetime", nullable=false)
     */
    protected $date;
    
    /**
     * Duration of the game
     * 
     * @Column (name="time", type="integer", nullable=false)
     */
    protected $time;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Relations
     * 
     * -------------------------------------------------------------------------
     */
    
    /**
     * The person that has played the game
     * 
     * @ManyToOne (targetEntity="person", inversedBy="scores")
     * @JoinColumn (name="idperson", referencedColumnName="idperson", nullable=false, onDelete="CASCADE")
     */
    protected $person;
    
    /**
     * @ManyToOne (targetEntity="session", inversedBy="scores")
     * @JoinColumn (name="idsession", referencedColumnName="idsession", nullable=false, onDelete="CASCADE")
     */
    protected $session;
    
    /**
     * Answers that the player has given in this game.
     * 
     * @ManyToMany (targetEntity="answer", inversedBy="scores")
     * @JoinTable (name="score_answer",
     *      joinColumns={@JoinColumn(name="idscore", referencedColumnName="idscore", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="idanswer", referencedColumnName="idanswer", onDelete="CASCADE")}
     * )
     */
    protected $answers;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Methods
     * 
     * -------------------------------------------------------------------------
     */
    
    public function __construct($person, $session, $points, $date, $time, $isPlayed = true) {
        $this->id = 0;
        $this->person = $person;
        $this->isPlayed = $isPlayed;
        $this->points = $points;
        $this->date = $date;
        $this->time = $time;
        $this->session = $session;  
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }
    //--------------------------------------------------------------------------
    public function getId() {
        return $this->id;
    }
    //--------------------------------------------------------------------------
    public function getPerson() {
        return $this->person;
    }
    public function setPerson($person) {
        $this->person = $person;
    }
    //--------------------------------------------------------------------------
    public function getIsPlayed() {
        return $this->isPlayed;
    }
    public function setIsPlayed($isPlayed) {
        $this->isPlayed = $isPlayed;
    }
    //--------------------------------------------------------------------------
    public function getPoints() {
        return $this->points;
    }
    public function setPoints($points) {
        $this->points = $points;
    }
    //--------------------------------------------------------------------------
    public function getDate() {
        return $this->date;
    }
    public function setDate($date) {
        $this->date = $date;
    }
    //--------------------------------------------------------------------------
    public function getTime() {
        return $this->time;
    }
    public function setTime($time) {
        $this->time = $time;
    }
    //--------------------------------------------------------------------------
    public function getSession() {
        return $this->session;
    }
    public function setSession($session) {
        $this->session = $session;
    }
    public function getInviter() {
        return $this->session->getPerson();
    }
    //--------------------------------------------------------------------------
    public function getQuestions() {
        return $this->session->getQuestions();
    }
    public function addQuestion($question) {
        $this->session->addQuestion($question);
    }
        public function getCorrectAnswers() {
        return $this->session->getCorrectAnswers();
    }
    //--------------------------------------------------------------------------
    public function getAnswers() {
        return $this->answers;
    }
    public function addAnswer($answer) {
        $this->answers->add($answer);
    }
    public function addAnswers($answers) {
        foreach($answers as $answer){
            if ($answer != NULL){
                $this->answers->add($answer);
            }
        }
    }
    //--------------------------------------------------------------------------
    public function getCategories() {
        return $this->session->getCategories();
    }
    //--------------------------------------------------------------------------
    public function getPersonName() {
        return $this->person->getName();
    }
    public function getPersonUsername() {
        return $this->person->getUsername();
    }
    public function getPersonEmail() {
        return $this->person->getEmail();
    }
    public function isPlayer() {
        return $this->person->isPlayer();
    }
    public function isModerator() {
        return $this->person->isModerator();
    }
    public function isAdmin() {
        return $this->person->isAdmin();
    }
}