<?php

namespace Entity;

/**
 * Description of Report
 * Report of a mistake in a question, reported by a person.
 * A report for a given question can be either for the question itself, or for one of its answers.
 * A moderator can handle a report by reading its text and editing the question / answer.
 * 
 * @author Alexa
 * @Entity
 * @Table (name="report")
 */
class Report {
    /**
     * @Id
     * @Column (name="idreport", type="integer", nullable=false)
     * @GeneratedValue
     */
    protected $id;
    
    /**
     * Contents of a report
     * 
     * @Column (name="text", type="text", nullable=false)
     */
    protected $text;
    
    /**
     * Whether the report has been handled by a moderator
     * @Column (name="ishandled", type="boolean", nullable=false)
     */
    protected $isHandled = false;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Relations
     * 
     * -------------------------------------------------------------------------
     */
    
    /**
     * The question that the report is for
     * 
     * @ManyToOne (targetEntity="question", inversedBy="reports")
     * @JoinColumn (name="idquestion", referencedColumnName="idquestion", nullable=false, onDelete="CASCADE")
     */
    protected $question;
    
    /**
     * The person that has reported a mistake
     * 
     * @ManyToOne (targetEntity="person", inversedBy="reports")
     * @JoinColumn (name="idperson", referencedColumnName="idperson", onDelete="SET NULL")
     */
    protected $person;
    
    /* -------------------------------------------------------------------------
     * 
     *                                 Methods
     * 
     * -------------------------------------------------------------------------
     */
    
    public function __construct($text, $question, $person) {
        $this->id = 0;
        $this->text = $text;
        $this->question = $question;
        $this->person = $person;
    }
    //--------------------------------------------------------------------------
    public function getId() {
        return $this->id;
    }
    //--------------------------------------------------------------------------
    public function getText() {
        return $this->text;
    }
    public function setText($text) {
        $this->text = $text;
    }
    //--------------------------------------------------------------------------
    public function isHandled() {
        return $this->isHandled;
    }
    public function setIsHandled($isHandled) {
        $this->isHandled = $isHandled;
    }
    //--------------------------------------------------------------------------
    public function getQuestion() {
        return $this->question;
    }
    public function setQuestion($question) {
        $this->question = $question;
    }
    public function getQuestionText() {
        return $this->question->getText();
    }
    public function getQuestionAnswers() {
        return $this->question->getAnswers();
    }
    public function getQuestionCorrectAnswer() {
        return $this->question->getCorrectAnswer();
    }
    //--------------------------------------------------------------------------
    public function getPerson() {
        return $this->person;
    }
    public function setPerson($person) {
        $this->person = $person;
    }
    public function getPersonName() {
        if ($this->person == NULL) {
            return "nepoznati korisnik";
        }
        return $this->person->getName();
    }
    public function getPersonUsername() {
        if ($this->person == NULL) {
            return "nepoznati korisnik";
        }
        return $this->person->getUsername();
    }
    public function getPersonEmail() {
        if ($this->person == NULL) {
            return "nepoznati korisnik";
        }
        return $this->person->getEmail();
    }
    public function isPlayer() {
        if ($this->person == NULL) {
            return false;
        }
        return $this->person->isPlayer();
    }
    public function isModerator() {
        if ($this->person == NULL) {
            return false;
        }
        return $this->person->isModerator();
    }
    public function isAdmin() {
        if ($this->person == NULL) {
            return false;
        }
        return $this->person->isAdmin();
    }
}
