var number = 10;
var qnum = 10;
var timeWindow = null;
var timer;
var seconds;

function put(i, j, force = 0){
	document.getElementById(i + "answered").value = j;
	document.getElementById(i + "question").style.display = "none";
	
	if (i < qnum) {
		document.getElementById((i+1) + "question").style.display = "";
	}
	
	number--;
	var sec = parseInt(document.getElementById("sec").value);
	
	if (force === 1){
		sec = sec + 10;
	}
	else {
		sec = sec + 10;
		if (timer > 0){
			sec = sec - (timer / 1000);
		}
	}
	
	document.getElementById("sec").value = sec;
	if (number === 0){
		clearInterval(timeWindow);
		document.getElementById("CountDownTimer").style.display = "none";
		document.getElementById("questionNumber").style.display = "none";
		document.getElementById("form-game").submit();
	}
	else{
		startTimer();
	}
	
	return false;
}

function writeTime() {   
	timer = timer - 10;
	if (timer < 0) {
		put(qnum-number+1, 5);
	}
	if(timer % 1000 === 0) {
		restart(timer / 1000);
	}
}

function startTimer(n = 0) {
	if (timeWindow !== null) {            
		restart();
		clearInterval(timeWindow);
	}
	
	if (n !== 0){
		
		number = qnum = n;
	}
	
	timer = 10000;
	$("#questionNumber").html("<h2>" + (qnum - number + 1) + " / " + qnum + "</h2>");
	timeWindow = setInterval(writeTime, 10);
}

$(window).blur(function() {
    document.getElementById("CountDownTimer").style.display = "none";
	while (number > 0){
            put(qnum-number+1, 5, 1);
        }
});

function setColor(secondsColor) {
	$("#CountDownTimer").TimeCircles({ 
		time: { 
			Days: { show: false }, 
			Hours: { show: false }, 
			Minutes: { show: false }, 
			Seconds: { 
				text: "",
				color: secondsColor,
			}
		}
	});
	$("#CountDownTimer").TimeCircles({count_past_zero: false});
}


function restart(seconds = 10) {
	if (seconds > 6) {
		color = "#00ff00";
	}
	else if (seconds > 3) {
		color = "#ffa500";
	}
	else {
		color = "#ff0000";
	}
	setColor(color);	
	$(".example").TimeCircles().rebuild();
	$("#CountDownTimer").attr("data-timer", seconds);
	if(seconds === 10) {
		$("#CountDownTimer").TimeCircles().restart();
	}
}