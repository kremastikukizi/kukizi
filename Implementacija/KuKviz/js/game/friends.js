$('#dataTable').dataTable( {
	"columns": [
		null,
		null,
		{ "orderable": false }
	],
	"language": {
		"lengthMenu": "Prikaži _MENU_ redova",
		"search": "Pretraga:",
		"info": "Prikazuje se _PAGE_ od _PAGES_ stranica",
		"paginate": {
			"previous": "Prethodna",
			"next": "Sledeća"
		},
		"emptyTable": "Ne postoji nijedan korisnik",
		"infoEmpty": ""
	}
} );