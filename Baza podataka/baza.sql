-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 08, 2018 at 11:56 AM
-- Server version: 5.7.20-log
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kukviz`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
CREATE TABLE IF NOT EXISTS `answer` (
  `idanswer` int(11) NOT NULL AUTO_INCREMENT,
  `idquestion` int(11) NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `iscorrect` tinyint(1) NOT NULL,
  PRIMARY KEY (`idanswer`),
  KEY `IDX_DADD4A251C220143` (`idquestion`)
) ENGINE=InnoDB AUTO_INCREMENT=605 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`idanswer`, `idquestion`, `text`, `iscorrect`) VALUES
(1, 1, 'Tesla', 0),
(2, 1, 'Edison', 0),
(3, 1, 'Bel', 1),
(4, 1, 'Vat', 0),
(5, 2, 'Faradej', 0),
(6, 2, 'Farenhajt', 0),
(7, 2, 'Fulton', 1),
(8, 2, 'Fuko', 0),
(9, 3, 'Nemačke', 0),
(10, 3, 'Češke', 0),
(11, 3, 'Poljske', 1),
(12, 3, 'Austrije', 0),
(13, 4, 'Luj Paster', 1),
(14, 4, 'Aleksandar Fleming', 0),
(15, 4, 'Marija Kiri', 0),
(16, 4, 'Johan Mendel', 0),
(17, 5, 'letenja', 0),
(18, 5, 'otvorenog prostora', 0),
(19, 5, 'visine', 0),
(20, 5, 'stranaca', 1),
(21, 6, 'Čarls Dikens', 0),
(22, 6, 'Viljem Normanski', 0),
(23, 6, 'Edgar Alan Po', 0),
(24, 6, 'Viljem Šekspir', 1),
(25, 7, 'Baruha de Spinoze', 0),
(26, 7, 'Imanuela Kanta', 0),
(27, 7, 'Renea Dekarta', 1),
(28, 7, 'Fridriha Ničea', 0),
(29, 8, 'Hajdn', 0),
(30, 8, 'Betoven', 1),
(31, 8, 'Bah', 0),
(32, 8, 'Mocart', 0),
(33, 9, 'Parizu', 0),
(34, 9, 'Kanu', 0),
(35, 9, 'Sankt Petersburgu', 1),
(36, 9, 'Berlinu', 0),
(37, 10, 'kvantiteta', 0),
(38, 10, 'mediokriteta', 0),
(39, 10, 'relativiteta', 1),
(40, 10, 'kvaliteta', 0),
(41, 11, 'implementacija', 0),
(42, 11, 'imanacija', 0),
(43, 11, 'imputacija', 0),
(44, 11, 'implantacija', 1),
(45, 12, 'mobilnu telefoniju', 0),
(46, 12, 'veoma važne osobe', 1),
(47, 12, 'veoma opasne osobe', 0),
(48, 12, '\"vrati ili plati\"', 0),
(49, 13, 'Majkl Džordan', 0),
(50, 13, 'Mik Džeger', 1),
(51, 13, 'Majkl Džekson', 0),
(52, 13, 'Džeri Luis', 0),
(53, 14, 'nehata', 0),
(54, 14, 'koristoljublja', 0),
(55, 14, 'milosrđa', 1),
(56, 14, 'osvete', 0),
(57, 15, 'podmukla', 1),
(58, 15, 'načitana', 0),
(59, 15, 'štedljiva', 0),
(60, 15, 'umorna', 0),
(61, 16, 'glumci i režiseri', 0),
(62, 16, 'političari', 0),
(63, 16, 'muzičari', 0),
(64, 16, 'novinari', 1),
(65, 17, 'mikrobiologija', 0),
(66, 17, 'citologija', 1),
(67, 17, 'homologija', 0),
(68, 17, 'sistologija', 0),
(69, 18, 'plava', 1),
(70, 18, 'zelena', 0),
(71, 18, 'žuta', 0),
(72, 18, 'ljubičasta', 0),
(73, 19, 'plašljiv', 0),
(74, 19, 'milosrdan', 1),
(75, 19, 'lakomislen', 0),
(76, 19, 'nervozan', 0),
(77, 20, 'Latinski', 0),
(78, 20, 'Panamski', 1),
(79, 20, 'Bermudski', 0),
(80, 20, 'Južnoamerički', 0),
(81, 21, 'stetoskop', 0),
(82, 21, '\"crna kutija\"', 1),
(83, 21, '\"bela knjiga\"', 0),
(84, 21, 'sonar', 0),
(85, 22, 'Edgara Alana Poa', 0),
(86, 22, 'Agate Kristi', 0),
(87, 22, 'Artura Konana Dojla', 1),
(88, 22, 'Sidnija Šeldona', 0),
(89, 23, 'sporost i tromost', 0),
(90, 23, 'snagu i jačinu', 0),
(91, 23, 'inteligenciju i okretnost', 0),
(92, 23, 'slabost i ranjivost', 1),
(93, 24, 'dinamit', 1),
(94, 24, 'teleskop', 0),
(95, 24, 'sunčeve pege', 0),
(96, 24, 'barut', 0),
(97, 25, 'Šekspir', 0),
(98, 25, 'Tolkin', 1),
(99, 25, 'Hobit', 0),
(100, 25, 'Gete', 0),
(101, 26, '\"Zločin i kazna\"', 0),
(102, 26, '\"Majstor i Margarita\"', 0),
(103, 26, '\"Ana Karenjina\"', 1),
(104, 26, '\"Evgenije Onjegin\"', 0),
(105, 27, 'uspeh', 0),
(106, 27, 'neuspeh', 1),
(107, 27, 'stagnaciju', 0),
(108, 27, 'slavlje', 0),
(109, 28, 'pritisak', 0),
(110, 28, 'radioaktivnost', 1),
(111, 28, 'zagađenost', 0),
(112, 28, 'frekvencija', 0),
(113, 29, '1 cm', 0),
(114, 29, '2,5 cm', 1),
(115, 29, '5 cm', 0),
(116, 29, '60 cm', 0),
(117, 30, 'etnografija', 0),
(118, 30, 'grafistika', 0),
(119, 30, 'grafoskopija', 0),
(120, 30, 'kaligrafija', 1),
(121, 31, 'Beograd', 0),
(122, 31, 'Melburn', 0),
(123, 31, 'Kopenhagen', 0),
(124, 31, 'Njujork', 1),
(125, 32, 'košarci', 0),
(126, 32, 'tenisu', 1),
(127, 32, 'odbojci', 0),
(128, 32, 'rukometu', 0),
(129, 33, 'brod u opasnosti', 0),
(130, 33, 'u pomoć, tonemo', 0),
(131, 33, 'spasite naše duše', 1),
(132, 33, 'svi na palubu', 0),
(133, 34, 'u jednog boga', 1),
(134, 34, 'ni u jednog boga', 0),
(135, 34, 'u više bogova', 0),
(136, 34, 'u dva boga', 0),
(137, 35, 'Drugi svetski rat', 0),
(138, 35, 'Treći svetski rat', 0),
(139, 35, 'Balkanski rat', 0),
(140, 35, 'Prvi svetski rat', 1),
(141, 36, 'Indije', 1),
(142, 36, 'Vijetnama', 0),
(143, 36, 'Nepala', 0),
(144, 36, 'Tibeta', 0),
(145, 37, 'treći svet', 0),
(146, 37, 'drugi svet', 0),
(147, 37, 'treći stalež', 0),
(148, 37, 'treći rajh', 1),
(149, 38, 'Norveška', 0),
(150, 38, 'Švedska', 0),
(151, 38, 'Litvanija', 1),
(152, 38, 'Finska', 0),
(153, 39, 'nalazišta srebra', 0),
(154, 39, 'neophodni sastojci za pripremu ruma', 0),
(155, 39, 'južno voće', 1),
(156, 39, 'ništa od navedenog', 0),
(157, 40, 'mandolina', 1),
(158, 40, 'papalina', 0),
(159, 40, 'konkubina', 0),
(160, 40, 'artolina', 0),
(161, 41, 'Rimski Korsakov', 0),
(162, 41, 'Gaj Julije Cezar', 0),
(163, 41, 'Eneja', 0),
(164, 41, 'Romul i Rem', 1),
(165, 42, 'Novog Zelanda', 0),
(166, 42, 'Havaja', 0),
(167, 42, 'Australije', 1),
(168, 42, 'Kanade', 0),
(169, 43, 'Glazgovu', 0),
(170, 43, 'Edinburgu', 0),
(171, 43, 'Bremenu', 0),
(172, 43, 'Minhenu', 1),
(173, 44, 'Jelene', 1),
(174, 44, 'Atine', 0),
(175, 44, 'Zevsa', 0),
(176, 44, 'Jovane', 0),
(177, 45, 'tagrada', 0),
(178, 45, 'zabrana', 1),
(179, 45, 'dozvola', 0),
(180, 45, 'pristanak', 0),
(181, 46, 'Ženevi', 0),
(182, 46, 'Briselu', 0),
(183, 46, 'Strazburu', 0),
(184, 46, 'Njujorku', 1),
(185, 47, 'neizvesnog ishoda', 0),
(186, 47, 'povratni', 0),
(187, 47, 'kratkotrajni', 0),
(188, 47, 'nepovratni', 1),
(189, 48, 'pečurki', 0),
(190, 48, 'pića', 0),
(191, 48, 'kolača', 1),
(192, 48, 'čokolade', 0),
(193, 49, 'antički bog igre', 0),
(194, 49, 'opseg', 1),
(195, 49, 'čaša', 0),
(196, 49, 'preteča foto-aparata', 0),
(197, 50, 'Džon Kenedi', 0),
(198, 50, 'Abraham Linkoln', 1),
(199, 50, 'Teodor Ruzvelt', 0),
(200, 50, 'Martin Luter King', 0),
(201, 51, 'Dobro rodila', 0),
(202, 51, 'Nova vrsta', 0),
(203, 51, 'Samonikla', 0),
(204, 51, 'Dragocena', 1),
(205, 52, '\"Kocka je bačena\"', 1),
(206, 52, '\"Zar i ti sine Brute\"', 0),
(207, 52, '\"Hrabrima sreća pomaže\"', 0),
(208, 52, '\"Dođoh, videh, pobedih\"', 0),
(209, 53, 'carica Milica', 0),
(210, 53, 'Majka Tereza', 0),
(211, 53, 'princeza Dajana', 0),
(212, 53, 'carica Jelena', 1),
(213, 54, 'u Srbiji', 0),
(214, 54, 'u Sao Paolu', 0),
(215, 54, 'na Švarcvaldu', 1),
(216, 54, 'na Kilimandžaru', 0),
(217, 55, 'inženjeri', 0),
(218, 55, 'kosmonauti', 0),
(219, 55, 'lekari', 1),
(220, 55, 'filozofi', 0),
(221, 56, 'vat', 0),
(222, 56, 'amper', 1),
(223, 56, 'džul', 0),
(224, 56, 'om', 0),
(225, 57, 'glumac', 0),
(226, 57, 'slikar', 1),
(227, 57, 'fizičar', 0),
(228, 57, 'španski premijer', 0),
(229, 58, 'oblast u Rusiji', 0),
(230, 58, 'reka', 0),
(231, 58, 'duvački instrument', 1),
(232, 58, 'planina', 0),
(233, 59, 'rasa psa', 1),
(234, 59, 'enciklopedija', 0),
(235, 59, 'ostrvo', 0),
(236, 59, 'oblast u Kanadi', 0),
(237, 60, 'Jemen', 0),
(238, 60, 'Chicago', 0),
(239, 60, 'Istambul', 1),
(240, 60, 'Kairo', 0),
(241, 61, 'Lej', 0),
(242, 61, 'Lev', 0),
(243, 61, 'Real', 0),
(244, 61, 'Jen ', 1),
(245, 62, 'tražeći kuću', 0),
(246, 62, 'tražeći pomorski put za Indiju', 1),
(247, 62, 'bežeći od neprijatelja', 0),
(248, 62, 'pišući vest', 0),
(249, 63, 'Tihom okeanu', 0),
(250, 63, 'Atlantskom okeanu', 1),
(251, 63, 'Indijskom okeanu', 0),
(252, 63, 'Severnom ledenom moru', 0),
(253, 64, 'pedagog', 0),
(254, 64, 'andragog', 0),
(255, 64, 'psiholog', 1),
(256, 64, 'istoričar', 0),
(257, 65, 'World World World', 0),
(258, 65, 'World Wide Web', 1),
(259, 65, 'vremensku prognozu', 0),
(260, 65, 'programski jezik', 0),
(261, 66, 'aritmija', 0),
(262, 66, 'strah', 0),
(263, 66, 'uzbuđenje', 0),
(264, 66, 'tahikardija', 1),
(265, 67, 'Aja Sofija', 0),
(266, 67, 'Bajrakli džamija', 1),
(267, 67, 'Sultan Selimova džamija', 0),
(268, 67, 'Begova džamija', 0),
(269, 68, 'crtanje', 0),
(270, 68, 'skiciranje', 0),
(271, 68, 'dizajniranje', 0),
(272, 68, 'piktografija', 1),
(273, 69, 'Tuluz-Lotrek', 0),
(274, 69, 'Dega', 0),
(275, 69, 'van Gog', 1),
(276, 69, 'Mone', 0),
(277, 70, 'vrsta eksploziva', 0),
(278, 70, '\"kobajagi\" lek', 1),
(279, 70, 'malo mesto', 0),
(280, 70, 'plastična igračka', 0),
(281, 71, 'španski nacionalni junak', 0),
(282, 71, 'planina u Rusiji', 0),
(283, 71, 'slika na zidu', 1),
(284, 71, 'pritoka Save', 0),
(285, 72, 'jugo', 0),
(286, 72, 'maestral', 0),
(287, 72, 'fen', 1),
(288, 72, 'široko', 0),
(289, 73, 'Branko Ćopić', 0),
(290, 73, 'Ivo Andrić', 1),
(291, 73, 'Dobrica Ćosić', 0),
(292, 73, 'Mika Antić', 0),
(293, 74, 'veliki post', 0),
(294, 74, 'ramazan', 1),
(295, 74, 'bajram', 0),
(296, 74, 'mezet', 0),
(297, 75, 'devetom', 0),
(298, 75, 'jedanaestom', 0),
(299, 75, 'desetom', 1),
(300, 75, 'dvanaestom', 0),
(301, 76, 'parfimerija', 0),
(302, 76, 'opera', 1),
(303, 76, 'vinarija', 0),
(304, 76, 'košarkaška dvorana', 0),
(305, 77, 'presto', 0),
(306, 77, 'moderato', 0),
(307, 77, 'alegro', 0),
(308, 77, 'lento', 0),
(309, 78, 'bakterija', 0),
(310, 78, 'gljiva', 0),
(311, 78, 'oblaka', 1),
(312, 78, 'političara', 0),
(313, 79, 'bipolarnosti', 0),
(314, 79, 'bifurkaciji', 1),
(315, 79, 'dehidraciji', 0),
(316, 79, 'paralokaciji', 0),
(317, 80, 'indijanski', 0),
(318, 80, 'portugalski', 1),
(319, 80, 'španski', 0),
(320, 80, 'brazilijanski', 0),
(321, 81, 'Etna', 1),
(322, 81, 'Vezuv', 0),
(323, 81, 'Stromboli', 0),
(324, 81, 'Lipari', 0),
(325, 82, 'slepila', 0),
(326, 82, 'feminizma', 0),
(327, 82, 'stradanja', 0),
(328, 82, 'pravde', 1),
(329, 83, 'reka', 0),
(330, 83, 'država', 1),
(331, 83, 'planina', 0),
(332, 83, 'fotomodel', 0),
(333, 84, 'didaktika', 0),
(334, 84, 'dikcija', 1),
(335, 84, 'dikciza', 0),
(336, 84, 'dihotomija', 0),
(337, 85, 'Tamiš', 0),
(338, 85, 'Temišvar', 1),
(339, 85, 'Tisa', 0),
(340, 85, 'Turnu Severin', 0),
(341, 86, 'horoskopske znake', 0),
(342, 86, 'astrološke simbole', 0),
(343, 86, 'svemir', 1),
(344, 86, 'astrološke kuće', 0),
(345, 87, 'muzičar', 0),
(346, 87, 'ikonograf', 0),
(347, 87, 'pesnik', 1),
(348, 87, 'kompozitor', 0),
(349, 88, 'Pi', 1),
(350, 88, 'e', 0),
(351, 88, 'log', 0),
(352, 88, 'ln', 0),
(353, 89, '365 puta', 0),
(354, 89, 'dva puta', 0),
(355, 89, 'jednom', 1),
(356, 89, 'više puta, u zavisnosti od godišnjeg doba', 0),
(357, 90, 'Moskvi', 0),
(358, 90, 'Parizu', 0),
(359, 90, 'Londonu', 1),
(360, 90, 'Sankt Petersburgu', 0),
(361, 91, 'dioptrija', 0),
(362, 91, 'miopija', 1),
(363, 91, 'pivopija', 0),
(364, 91, 'biopija', 0),
(365, 92, 'Au', 1),
(366, 92, 'Zn', 0),
(367, 92, 'Ag', 0),
(368, 92, 'Zl', 0),
(369, 93, 'Hukujama', 0),
(370, 93, 'Hirošima', 0),
(371, 93, 'Fukušima', 1),
(372, 93, 'Fudžijama', 0),
(373, 94, 'Sidnej', 0),
(374, 94, 'Melburn', 0),
(375, 94, 'Kingston', 0),
(376, 94, 'Kanbera', 1),
(377, 95, 'sira', 1),
(378, 95, 'muzičkog instrumenta', 0),
(379, 95, 'horske pesme', 0),
(380, 95, 'leptira', 0),
(381, 96, 'modni kreator', 0),
(382, 96, 'fudbalski trener', 1),
(383, 96, 'pesnik', 0),
(384, 96, 'baletan', 0),
(385, 97, 'Džejms Kameron', 1),
(386, 97, 'Martin Skorceze', 0),
(387, 97, 'Roman Polanski', 0),
(388, 97, 'Stiven Spilberg', 0),
(389, 98, 'slikar', 1),
(390, 98, 'pisac', 0),
(391, 98, 'arhitekta', 0),
(392, 98, 'kompozitor', 0),
(393, 99, 'vajar', 0),
(394, 99, 'košarkaš', 0),
(395, 99, 'pevač', 0),
(396, 99, 'kuvar', 1),
(397, 100, 'Laza Kostić', 1),
(398, 100, 'Vladislav Petković Dis', 0),
(399, 100, 'Vojislav Ilić', 0),
(400, 100, 'Laza Lazarević', 0),
(401, 101, 'bambusom', 1),
(402, 101, 'eukaliptusom', 0),
(403, 101, 'bananama', 0),
(404, 101, 'bonsaiem', 0),
(405, 102, 'SAD', 0),
(406, 102, 'Rusija', 0),
(407, 102, 'Brazil', 0),
(408, 102, 'Indija', 1),
(409, 103, 'Njujorku', 0),
(410, 103, 'Vašingtonu', 0),
(411, 103, 'Los Anđelesu', 0),
(412, 103, 'San Francisku', 1),
(413, 104, 'Portugala', 0),
(414, 104, 'Maroka', 0),
(415, 104, 'Tunisa', 0),
(416, 104, 'Španije', 1),
(417, 105, 'Aleksis de Tokvil', 0),
(418, 105, 'Markiz de Sad', 0),
(419, 105, 'Žil Vern', 1),
(420, 105, 'Onore de Balzak', 0),
(421, 106, 'Africi', 1),
(422, 106, 'Aziji', 0),
(423, 106, 'Južnoj Americi', 0),
(424, 106, 'Evropi', 0),
(425, 107, 'Etiopija Siti', 0),
(426, 107, 'Kartum', 0),
(427, 107, 'Najrobi', 0),
(428, 107, 'Adis Abeba', 1),
(429, 108, 'Kragujevac', 0),
(430, 108, 'Kijev', 0),
(431, 108, 'Kairo', 1),
(432, 108, 'Kopenhagen', 0),
(433, 109, 'Sicilija', 0),
(434, 109, 'Velika Britanija', 1),
(435, 109, 'Krit', 0),
(436, 109, 'Island', 0),
(437, 110, 'Hong Kong', 1),
(438, 110, 'Vijetnam', 0),
(439, 110, 'Inodnezija', 0),
(440, 110, 'Japan', 0),
(441, 111, 'Islandu', 0),
(442, 111, 'Danskoj', 1),
(443, 111, 'Švedskoj', 0),
(444, 111, 'Kanadi', 0),
(445, 112, 'Milano', 0),
(446, 112, 'Berlin', 0),
(447, 112, 'Ženevu', 0),
(448, 112, 'Sankt Petersburg', 1),
(449, 113, 'deo litosfere', 0),
(450, 113, 'omot za svesku', 0),
(451, 113, 'šumska oblast u Amazoniji', 0),
(452, 113, 'sloj u stratosferi', 1),
(453, 114, 'jednake temperature', 0),
(454, 114, 'jednakog pritiska', 1),
(455, 114, 'jednakog zračenja', 0),
(456, 114, 'jednake morske visine', 0),
(457, 115, 'Halejeva kometa', 1),
(458, 115, 'planeta Pluton', 0),
(459, 115, 'sazvežđe planete Mars', 0),
(460, 115, 'zvezda Vega', 0),
(461, 116, '1800 metara', 0),
(462, 116, '1700 metara', 0),
(463, 116, '1600 metara', 1),
(464, 116, '1500 metara', 0),
(465, 117, 'kit', 0),
(466, 117, 'delfin', 0),
(467, 117, 'ajkula', 1),
(468, 117, 'slepi miš', 0),
(469, 118, 'Aleksandrije', 0),
(470, 118, 'Rima', 1),
(471, 118, 'Kartagine', 0),
(472, 118, 'Atine', 0),
(473, 119, 'Turski', 0),
(474, 119, 'Namesnički', 0),
(475, 119, 'Vidovdanski', 0),
(476, 119, 'Sretenjski', 1),
(477, 120, 'Orašcu', 0),
(478, 120, 'Takovu', 1),
(479, 120, 'Kragujevcu', 0),
(480, 120, 'Kruševcu', 0),
(481, 121, '1878.', 1),
(482, 121, '1814.', 0),
(483, 121, '1918.', 0),
(484, 121, '1945.', 0),
(485, 122, 'Ukrajina', 0),
(486, 122, 'Avganistan', 1),
(487, 122, 'Azerbejdžan', 0),
(488, 122, 'Gruzija', 0),
(489, 123, 'Aleksandar Obrenović', 1),
(490, 123, 'Aleksandar Karađorđević', 0),
(491, 123, 'Milan Obrenović', 0),
(492, 123, 'Mihailo Obrenović', 0),
(493, 124, 'rat divova', 0),
(494, 124, 'rat ruža', 1),
(495, 124, 'rat dve lale', 0),
(496, 124, 'tridesetogodišni rat', 0),
(497, 125, 'Herodot', 1),
(498, 125, 'Lenjin', 0),
(499, 125, 'Homer', 0),
(500, 125, 'Ezop', 0),
(501, 126, '1989.', 0),
(502, 126, '1969.', 0),
(503, 126, '1961.', 1),
(504, 126, '1981.', 0),
(505, 127, 'Kinezi', 0),
(506, 127, 'Japanci', 1),
(507, 127, 'Rusi', 0),
(508, 127, 'Amerikanci', 0),
(509, 128, 'Barseloni', 0),
(510, 128, 'Parizu', 0),
(511, 128, 'Rimu', 0),
(512, 128, 'Madridu', 1),
(513, 129, 'Gabrijel Garsija Markez', 1),
(514, 129, 'Pablo Neruda', 0),
(515, 129, 'Erih Marija Remark', 0),
(516, 129, 'Šarl Bodler', 0),
(517, 130, '\"Glava šećera\"', 0),
(518, 130, '\"Koštana\"', 1),
(519, 130, '\"Zona Zamfirova', 0),
(520, 130, '\"Prvi put s ocem na jutrenje\"', 0),
(521, 131, 'Artura Remboa', 0),
(522, 131, 'Šarla Bodlera', 1),
(523, 131, 'Edgara Alan Poa', 0),
(524, 131, 'Milana Rakića', 0),
(525, 132, 'Đakomo Pučini', 0),
(526, 132, 'Ludvig van Betoven', 0),
(527, 132, 'Đuzepe Verdi', 1),
(528, 132, 'Volfgang Amadeus Mocart', 0),
(529, 133, '\"New wave\"', 1),
(530, 133, '\"Old boy\"', 0),
(531, 133, '\"Evergreen\"', 0),
(532, 133, '\"Rockin’ and rollin’\"', 0),
(533, 134, '\"Tihi Don\"', 0),
(534, 134, '\"Stranac\"', 0),
(535, 134, '\"Kockar\"', 1),
(536, 134, '\"Jedan dan Ivana Denisoviča\"', 0),
(537, 135, 'Džeri Luis', 0),
(538, 135, 'Čabi Čeker', 0),
(539, 135, 'Džordž Harison', 1),
(540, 135, 'Čak Beri', 0),
(541, 136, 'Mikelanđelo', 1),
(542, 136, 'Leonardo da Vinči', 0),
(543, 136, 'Van Gog', 0),
(544, 136, 'Ticijan', 0),
(545, 137, 'špageti vestern', 1),
(546, 137, 'njoki vestern', 0),
(547, 137, 'vestern donovi', 0),
(548, 137, 'vesternini', 0),
(549, 138, '\"Ex Ponto\"', 0),
(550, 138, '\"Tvrđavu\"', 1),
(551, 138, '\"Travničku hroniku\"', 0),
(552, 138, '\"Priču o kmetu Simanu\"', 0),
(553, 139, '1054.', 1),
(554, 139, '1517.', 0),
(555, 139, '1389.', 0),
(556, 139, '1492.', 0),
(557, 140, 'Veliku Britaniju', 0),
(558, 140, 'Sovjetski Savez', 0),
(559, 140, 'Francusku', 0),
(560, 140, 'Poljsku', 1),
(561, 141, 'blagostanju', 0),
(562, 141, 'miru', 0),
(563, 141, 'teroru', 1),
(564, 141, 'razvitku', 0),
(565, 142, 'Votergejt afere', 1),
(566, 142, 'Mariner afere', 0),
(567, 142, 'Apolo afere', 0),
(568, 142, 'Džemini afere', 0),
(569, 143, 'Petar I Karađorđević', 1),
(570, 143, 'Aleksandar Obrenović', 0),
(571, 143, 'Aleksandar Karađorđević', 0),
(572, 143, 'Mihailo Obrenović', 0),
(573, 144, 'Oktavijan Avgust', 0),
(574, 144, 'Aleksandar Makedonski', 0),
(575, 144, 'Hanibal Lektor', 0),
(576, 144, 'Gaj Julije Cezar', 1),
(577, 145, 'Francuske i Italije', 0),
(578, 145, 'Španije i Portugala', 0),
(579, 145, 'Engleske i Španije', 0),
(580, 145, 'Francuske i Engleske', 1),
(581, 146, 'župana', 0),
(582, 146, 'kneza', 1),
(583, 146, 'kralja', 0),
(584, 146, 'despota', 0),
(585, 147, '1900.', 0),
(586, 147, '1914.', 0),
(587, 147, '1908.', 1),
(588, 147, '1918.', 0),
(589, 148, '1914.', 0),
(590, 148, '1915.', 0),
(591, 148, '1918.', 1),
(592, 148, '1945.', 0),
(593, 149, '1918.', 0),
(594, 149, '1882.', 1),
(595, 149, '1888.', 0),
(596, 149, '1804.', 0),
(597, 150, 'Engleske i Francuske', 0),
(598, 150, 'Nemačke i SSSR-a', 0),
(599, 150, 'SAD i SSSR-a', 1),
(600, 150, 'SAD i Iraka', 0),
(601, 151, 'Drugi', 0),
(602, 151, 'je', 0),
(603, 151, 'tačan', 0),
(604, 151, 'lol', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `idcategory` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idcategory`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`idcategory`, `name`) VALUES
(1, 'Opšta kultura'),
(2, 'Istorija'),
(3, 'Geografija'),
(4, 'Nauka i matematika'),
(5, 'Umetnost'),
(6, 'Šelo');

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
CREATE TABLE IF NOT EXISTS `friends` (
  `idperson` int(11) NOT NULL,
  `idfriend` int(11) NOT NULL,
  PRIMARY KEY (`idperson`,`idfriend`),
  KEY `IDX_21EE706950617CDA` (`idperson`),
  KEY `IDX_21EE7069315301CD` (`idfriend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `idperson` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idperson`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`idperson`, `name`, `email`, `username`, `password`, `gender`, `type`) VALUES
(1, 'Natalija Yasir', 'yasir@yasir.com', 'yasir', 'yasir', 'female', 'admin'),
(2, 'Petar Đekanović', 'djekan@djekan.com', 'djekan', 'djekan', 'male', 'admin'),
(3, 'Miljan Zarubica', 'zabura@zabura.com', 'zabura', 'zabura', 'male', 'admin'),
(4, 'Aleksa Brkić', 'brks@brks.com', 'brks', 'brks', 'male', 'admin'),
(5, 'Miloš Matijašević', 'matke@matke.com', 'matke', 'matke', 'male', 'moderator'),
(6, 'Dejan Ćirić', 'cira@cira.com', 'cira', 'cira', 'male', 'player'),
(8, 'Player 2', 'p2@boi.com', 'p2', 'p2', 'male', 'player'),
(9, 'Player 3', 'p3@boi.com', 'p3', 'p3', 'female', 'player'),
(10, 'Player 4', 'p4@boi.com', 'p4', 'p4', 'male', 'player');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `idquestion` int(11) NOT NULL AUTO_INCREMENT,
  `idcategory` int(11) NOT NULL,
  `idperson` int(11) DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idquestion`),
  KEY `IDX_B6F7494EAC9951CC` (`idcategory`),
  KEY `IDX_B6F7494E50617CDA` (`idperson`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`idquestion`, `idcategory`, `idperson`, `text`) VALUES
(1, 4, 1, 'Ko je izmislio telefon?'),
(2, 4, 2, 'Ko je napravio prvi parobrod?'),
(3, 3, 3, 'Logor Aušvic se nalazio na prostoru današnje:'),
(4, 4, 4, 'Penicilin je otkrio/otkrila:'),
(5, 4, 1, 'Ksenofobija je strah od:'),
(6, 5, 2, 'Ko je napisao tragediju \"Hamlet\"?'),
(7, 1, 3, 'Cogito, ergo sum izjava je:'),
(8, 5, 4, 'Koji kompozitor je pred kraj života postao gluv?'),
(9, 5, 1, 'Muzej Ermitaž se nalazi u:'),
(10, 4, 2, 'Albert Ajnštajn je otkrio teoriju:'),
(11, 4, 3, 'Usađivanje tuđeg tkiva čoveku naziva se:'),
(12, 1, 4, 'Šta označava skraćenica VIP?'),
(13, 5, 1, 'Frontmen grupe Rolingstones zove se:'),
(14, 1, 2, 'Eutanazija je ubistvo iz:'),
(15, 1, 3, 'Perfidna osoba je:'),
(16, 1, 4, 'Pulicerovu nagradu dobijaju:'),
(17, 4, 1, 'Nauka o ćeliji je:'),
(18, 1, 2, 'Azurna boja je:'),
(19, 1, 3, 'Za nekog čoveka kažemo da je samarićanin kada je:'),
(20, 3, 4, 'Kanal koji spaja Atlantik i Pacifik naziva se:'),
(21, 4, 1, 'Uzroke avionskih udesa moguće je utvrditi pomoću:'),
(22, 5, 2, 'Šerlok Holms je kreacija:'),
(23, 1, 3, 'Ahilova peta je simbol za:'),
(24, 4, 4, 'Alfred Nobel je otkrio:'),
(25, 5, 1, 'Delo \"Gospodar prstenova\" napisao je:'),
(26, 5, 2, 'Vronski je lik iz dela:'),
(27, 1, 3, 'Debakl znači:'),
(28, 4, 4, 'Gajgerovim brojačem se meri:'),
(29, 4, 1, 'Dužina od jednog inča iznosi oko:'),
(30, 5, 2, 'Veština lepog i urednog pisanja naziva se:'),
(31, 3, 3, 'Velika jabuka je popularan naziv za:'),
(32, 1, 4, 'Dejvis kup je međunarodno takmičenje u:'),
(33, 1, 1, 'Doslovno značenje međunarodnog poziva za pomoć SOS je:'),
(34, 1, 2, 'Monoteizam je verovanje:'),
(35, 2, 3, 'Rat za podelu sveta između 1914. i 1918. poznat je kao:'),
(36, 2, 4, 'Mahatma Gandi se borio za nezavisnost:'),
(37, 2, 1, 'Naziv za razdoblje nacističke vladavine u Nemačkoj je:'),
(38, 3, 2, 'Koja od navedenih zemalja ne pripada Skandinaviji?'),
(39, 1, 3, 'Agrumi su:'),
(40, 5, 4, 'Muzički instrument je:'),
(41, 2, 1, 'Ko je po legendi osnovao Rim?'),
(42, 3, 2, 'Aboridžini su domorodačko stanovništvo:'),
(43, 1, 3, 'U kom gradu se održava Oktoberfest?'),
(44, 2, 4, 'Trojanski rat se vodio zbog:'),
(45, 1, 1, 'Tabu znači:'),
(46, 3, 2, 'Sedište UN se nalazi u:'),
(47, 4, 3, 'Ireverzibilni procesi su:'),
(48, 1, 4, 'Tiramisu je vrsta :'),
(49, 1, 1, 'Dijapazon je:'),
(50, 2, 2, 'Ropstvo u Sjedinjenim američkim državama je ukinuo:'),
(51, 4, 3, 'Ako je biljna vrsta autohtona, to znači da je ona:'),
(52, 2, 4, 'Cezareve reči kada je prešao reku Rubikon su:'),
(53, 1, 1, 'Jedina žena koja je boravila na Svetoj Gori je:'),
(54, 3, 2, 'Reka Dunav izvire:'),
(55, 1, 3, 'Hipokratovu zakletvu polažu:'),
(56, 4, 4, 'Jedinica za merenje jačine električne struje:'),
(57, 5, 1, 'Salvador Dali je:'),
(58, 1, 2, 'Oboa je:'),
(59, 1, 3, 'Njufaundlender je:'),
(60, 3, 4, 'Jedini grad koji se prostire na dva kontineta je:'),
(61, 3, 1, 'Novčana jedinica u Japanu je:'),
(62, 3, 2, 'Kolumbo je otkrio Ameriku:'),
(63, 3, 3, 'Bermudski trougao se nalazi u:'),
(64, 4, 4, 'Sigmund Frojd je:'),
(65, 4, 1, 'WWW je skraćenica za:'),
(66, 4, 2, 'Brzina otkucaja srca preko 100 do 240 u minuti je:'),
(67, 1, 3, 'Jedina očuvana džamija na teritoriji Beograda je:'),
(68, 5, 4, 'Izražavanje reči i ideja kroz crteže je:'),
(69, 5, 1, 'Autor poznate slike \"Suncokreti\" je:'),
(70, 4, 2, 'Placebo je:'),
(71, 1, 3, 'Mural je:'),
(72, 3, 4, 'Topao i suv vetar koji topi sneg u Alpima zove se:'),
(73, 5, 1, 'Roman \"Na Drini Ćuprija\" napisao je:'),
(74, 1, 2, 'Mesec posta u islamu zove se:'),
(75, 2, 3, 'U kom veku je nastala ćirilica?'),
(76, 1, 4, 'La Scala je najpoznatija svetska:'),
(77, 5, 1, 'Muzička oznaka za umereni tempo je:'),
(78, 4, 2, 'Kumulusi, stratusi, cirusi su vrste:'),
(79, 3, 3, 'Kada voda iz jedne reke odlazi u dva sliva, radi se o:'),
(80, 3, 4, 'Sluzbeni jezik u Brazilu je:'),
(81, 3, 1, 'Najviši aktivni vulkan u Evropi je:'),
(82, 1, 2, 'Žena sa povezom preko očiju i vagom u ruci je simbol:'),
(83, 3, 3, 'Mauricijus je:'),
(84, 1, 4, 'Način govora naziva se:'),
(85, 3, 1, 'Timisoara se na srpskom jeziku zove:'),
(86, 4, 2, 'Astronomija izučava:'),
(87, 5, 3, 'Miroslav Antić, poznati je srpski:'),
(88, 4, 4, '3,14 se još označava kao:'),
(89, 3, 1, 'Koliko puta dnevno se Zemlja obrne oko svoje ose?'),
(90, 3, 2, 'Temza je reka u:'),
(91, 4, 3, 'Koji je naučni termin za kratkovidost?'),
(92, 4, 4, 'Hemijska oznaka za zlato?'),
(93, 4, 1, 'Nuklearna elektrana oštećena zemljotresom u Japanu je:'),
(94, 3, 2, 'Glavni grad Australije je:'),
(95, 1, 3, 'Gauda je vrsta:'),
(96, 1, 4, 'Žoze Murinjo je:'),
(97, 5, 1, 'Film Avatar reţirao je:'),
(98, 5, 2, 'Sava Šumanović je naš poznati:'),
(99, 1, 3, 'Džejmi Oliver je poznati:'),
(100, 5, 4, 'Pesmu \"Santa Maria della Salute\" napisao je:'),
(101, 4, 1, 'Panda se pretežno hrani:'),
(102, 3, 2, 'Koja je najmnogoljudnija zemlja posle Kine?'),
(103, 3, 3, 'U kojem od ovih gradova možete videti Zlatni most?'),
(104, 3, 4, 'Katalonija je deo:'),
(105, 5, 1, 'Autor \"20.000 milja pod morem\" je:'),
(106, 3, 2, 'Na kom kontinentu se nalazi država Mali?'),
(107, 3, 3, 'Kako se zove glavni grad Etiopije?'),
(108, 3, 4, 'Koji od ovih gradova se nalazi najjužnije?'),
(109, 3, 1, 'Najveće evropsko ostrvo je:'),
(110, 3, 2, 'Bivša britanska kolonija u Kini je:'),
(111, 3, 3, 'Grenland pripada:'),
(112, 3, 4, 'Reka Neva protiče kroz:'),
(113, 4, 1, 'Ozonski omotač je:'),
(114, 4, 2, 'Izobare su linije na karti koje spajaju mesta:'),
(115, 4, 3, 'Nebesko telo vidljivo sa Zemlje svakih 76 godina je:'),
(116, 4, 4, 'Dužina jedne milje je oko:'),
(117, 4, 1, 'Koja od navedenih životinja nije sisar?'),
(118, 2, 2, 'Hanibal ante portas vikali su u panici stanovnici:'),
(119, 2, 3, 'Prvi ustav koji je donet u Srbiji je:'),
(120, 2, 4, 'Drugi srpski ustanak je počeo u:'),
(121, 2, 1, 'Berlinski kongres je održan:'),
(122, 2, 2, 'Koja od sledećih država nije bila deo SSSR-a:'),
(123, 2, 3, 'Dragom Mašin je bio oženjen:'),
(124, 2, 4, 'Rat dinastija u Engleskoj je poznat po nazivu:'),
(125, 2, 1, '\"Otac istorije\" je:'),
(126, 2, 2, 'Berlinski zid je sagrađen:'),
(127, 2, 3, 'Perl Harbur su napali:'),
(128, 5, 4, 'Muzej Prado se nalazi u:'),
(129, 5, 1, 'Roman \"Sto godina samoće\" napisao je:'),
(130, 5, 2, 'Mitke je lik iz dela:'),
(131, 5, 3, '\"Cveće zla\" je zbirka pesama:'),
(132, 5, 4, '\"Aidu\" je komponovao:'),
(133, 5, 1, 'Pokret kojim je pank muzika doživela procvat je:'),
(134, 5, 2, 'Koje delo je napisao Fjodor Mihailovič Dostojevski?'),
(135, 5, 3, 'Ko od navedenih je bio u sastavu grupe The Beatles:'),
(136, 5, 4, 'Sikstinsku kapelu je oslikao:'),
(137, 5, 1, 'Italijanski vestern filmovi su se popularno zvali:'),
(138, 5, 2, 'Ivo Andrić nije napisao:'),
(139, 2, 3, 'Rascepa hrišćanske crkve se dogodio:'),
(140, 2, 4, 'Drugi svetski rat počeo je napadom Nemačke na:'),
(141, 2, 1, 'Jakobinska diktatura je poznata po:'),
(142, 2, 2, 'Američki predsednik Nikson je podneo ostavku zbog:'),
(143, 2, 3, 'Srpski vladar koji nije ubijen u atentatu je:'),
(144, 2, 4, 'Veni, vidi, vici izgovorio je:'),
(145, 2, 1, 'Stogodišnji rat se vodio između:'),
(146, 2, 2, 'Mihailo Obrenović je imao titulu:'),
(147, 2, 3, 'Austrougarska je aneksirala BIH:'),
(148, 2, 4, 'Kraljevina SHS je osnovana:'),
(149, 2, 1, 'Srbija je postala kraljevina:'),
(150, 2, 2, 'Hladni rat se vodio između:'),
(151, 6, NULL, 'Šelo');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
CREATE TABLE IF NOT EXISTS `report` (
  `idreport` int(11) NOT NULL AUTO_INCREMENT,
  `idquestion` int(11) NOT NULL,
  `idperson` int(11) DEFAULT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ishandled` tinyint(1) NOT NULL,
  PRIMARY KEY (`idreport`),
  KEY `IDX_C42F77841C220143` (`idquestion`),
  KEY `IDX_C42F778450617CDA` (`idperson`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

DROP TABLE IF EXISTS `score`;
CREATE TABLE IF NOT EXISTS `score` (
  `idscore` int(11) NOT NULL AUTO_INCREMENT,
  `idperson` int(11) NOT NULL,
  `idsession` int(11) NOT NULL,
  `isPlayed` tinyint(1) NOT NULL,
  `points` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`idscore`),
  KEY `IDX_3299375150617CDA` (`idperson`),
  KEY `IDX_32993751F4087BA` (`idsession`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `score_answer`
--

DROP TABLE IF EXISTS `score_answer`;
CREATE TABLE IF NOT EXISTS `score_answer` (
  `idscore` int(11) NOT NULL,
  `idanswer` int(11) NOT NULL,
  PRIMARY KEY (`idscore`,`idanswer`),
  KEY `IDX_39070DFBE49C5B48` (`idscore`),
  KEY `IDX_39070DFBBE60E789` (`idanswer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE IF NOT EXISTS `session` (
  `idsession` int(11) NOT NULL AUTO_INCREMENT,
  `idperson` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsession`),
  KEY `IDX_D044D5D450617CDA` (`idperson`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `session_question`
--

DROP TABLE IF EXISTS `session_question`;
CREATE TABLE IF NOT EXISTS `session_question` (
  `idsession` int(11) NOT NULL,
  `idquestion` int(11) NOT NULL,
  PRIMARY KEY (`idsession`,`idquestion`),
  KEY `IDX_3D5B2926F4087BA` (`idsession`),
  KEY `IDX_3D5B29261C220143` (`idquestion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `FK_DADD4A251C220143` FOREIGN KEY (`idquestion`) REFERENCES `question` (`idquestion`) ON DELETE CASCADE;

--
-- Constraints for table `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `FK_21EE7069315301CD` FOREIGN KEY (`idfriend`) REFERENCES `person` (`idperson`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_21EE706950617CDA` FOREIGN KEY (`idperson`) REFERENCES `person` (`idperson`) ON DELETE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `FK_B6F7494E50617CDA` FOREIGN KEY (`idperson`) REFERENCES `person` (`idperson`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_B6F7494EAC9951CC` FOREIGN KEY (`idcategory`) REFERENCES `category` (`idcategory`) ON DELETE CASCADE;

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `FK_C42F77841C220143` FOREIGN KEY (`idquestion`) REFERENCES `question` (`idquestion`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C42F778450617CDA` FOREIGN KEY (`idperson`) REFERENCES `person` (`idperson`) ON DELETE SET NULL;

--
-- Constraints for table `score`
--
ALTER TABLE `score`
  ADD CONSTRAINT `FK_3299375150617CDA` FOREIGN KEY (`idperson`) REFERENCES `person` (`idperson`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_32993751F4087BA` FOREIGN KEY (`idsession`) REFERENCES `session` (`idsession`) ON DELETE CASCADE;

--
-- Constraints for table `score_answer`
--
ALTER TABLE `score_answer`
  ADD CONSTRAINT `FK_39070DFBBE60E789` FOREIGN KEY (`idanswer`) REFERENCES `answer` (`idanswer`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_39070DFBE49C5B48` FOREIGN KEY (`idscore`) REFERENCES `score` (`idscore`) ON DELETE CASCADE;

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `FK_D044D5D450617CDA` FOREIGN KEY (`idperson`) REFERENCES `person` (`idperson`) ON DELETE SET NULL;

--
-- Constraints for table `session_question`
--
ALTER TABLE `session_question`
  ADD CONSTRAINT `FK_3D5B29261C220143` FOREIGN KEY (`idquestion`) REFERENCES `question` (`idquestion`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_3D5B2926F4087BA` FOREIGN KEY (`idsession`) REFERENCES `session` (`idsession`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
